        -Index-
    -BISON
    -FLEX
    -kvf
    -HDF5
    -HYPSI
    -Folder Tree
    -TAGS output

summary, 
hypsi compile, bison and flex on minerva are late on aclocal version so used the local versions. 
I could try to check diff for the problem with kvf.


BISON
    Configure downloaded Bison (GNU Parser Generator) package:
        ./configure --prefix=/your/path/bison
#error finding aclocal-1.14, minerva is up to aclocal-1.10.1
    Compile Bison:

        make
        make check
        make install
    Note: check for any error messages.

FLEX
    Compiling and Installing Gnu Flex:
        ./configure --prefix=/your/path/bison
    Compile Flex:  
        make
        make check
        make install

KVF
    compiling kvf library:
	-having bison and flex properly installed
	-updated Makefile with:
		-compiler and flags
		- full path for Hypsi-local bison and flex
	make

HDF5
    download the binary precompiled version. After you have installed the
    binaries to their final destination, you can use these scripts
    (h5cc, h5fc, h5c++) to compile.  However, you must first:

    1) Run ./h5redeploy in the bin directory to change site specific paths in the scripts.

    2)optional: Edit each script and update the paths for the external libraries
       in LDFLAGS and CPPFLAGS. You need to know what you are doing!
       
HYPSI
    This is a parallel program using MPI. After kvf and hdf5 are properly installed we proceed to run the Makefile for Hypsi. At this stage we must update the USER_PATH in the Makefile to the directory where Hypsi will run.
    Test $PATH AND $LD_LIBRARY_PATH to check wether the compiler (ompi -gnu or intel, other) and the libraries (hdf5/lib kvf/) are set to run Hypsi. 
    
    You may add this into your .bashrc in $HOME directory, as follow:
        $export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/you/route/to/hypsi/hdf5/lib:/you/route/to/hypsi/kvf/
    Also, for users of MINERVA you may need to load compilers modules;
        $module avail
    One can update .bashrc runnig $source .bashrc. Minerva has gnu and intel compilers, an example will be;
         $module load ompi/1.6.4/gnu/4.3.4
         $module load ompi/1.6.4/intel/13.1
    Warning only one compiler must be load!
    
Folder tree
hypsi/ ---> hybridCode/ --> kvf-distrib/ (original files)
                      --> 43 original files (my version anyway)
                      --> input_file
                      --> doxygen.conf
                      --> job_PBS_Minerva
                      --> Makefile

       ---> guide_hypsi.pdf
       ---> hypsi_working.tar (backup)
       ---> kvf_working.tar   (backup)
       ---> README.md

       NOTE: development should include new files into this list.

Further.
- hypsi/ folder: contain main program and kvf.
- backup.tar: backup from gnu and intel installation on minerva August 2013, also all program files and input files.
- file: intel compiler: review of intel compiler use.
- file: open MPIc++: review of GNU mpi cpmpiler use.
- tconfig: input file for hypsi_main exec file(deprecated: see config_speed now), it was copy here to move to minerva.

- A new version: from Pete: was added in and this is the last stable version. Open ./hypsi folder for sources files.
- last kvf folder is inside too. HDF5 libraries are needed to run the code, set path also.
- A new folder Diagnostics was added for outputs and a viewer HDFview can be opened from there inside the folder.
- A file config_speed was included and is a test version.

-  HINTs:
    - I am doing development over hypsi (last version) as well as Pete so, we are making two different things from now on.
    - I proposed a documentation based on doxygen, so I started to write over the files and create updates on the html version.

TAGS   Output 
316 void output( const string& tag, int icycle )
317 {
323  strs << _hypsim->sim_params.hostzone;
325  zone_str = strs.str();
329  if( tag.find("ParticleData",0) != string::npos )
390  if( tag.find("SimulationParameters",0) != string::npos )
463  if( tag.find("Bfield",0) != string::npos )
480  if( tag.find("Efield",0) != string::npos )
498  if( tag.find("NVmoments",0) != string::npos )
538  if( tag.find("ParticleStats",0) != string::npos )
583  if( tag.find("ZoneFieldStats",0) != string::npos )

611 
612 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
613 // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
614 
615 } // end of output(const string& tag, int icycle)
    
