%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% HYPSI field file stitcher
%
% Script combines field dumps for several processors (e.g.
% data_bench_z0-9.hdf) into field variables such as B.x, E.x, M.protons.vx
% etc.
%
% Written by P W Gingell
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Data file locations
dest = '/scratch/pwg/'; % Data file path
fileprefix = 'data_bench'; % Data file root name

%What do you want me to read?
readB = true; % B field
readE = false; % E field
readM = false; % Particle moments n, v

% Field dump cadence (timesteps between data dumps)
% Note: currently assumes the same cadence for all dumped fields
fcadence = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read file zero for domain parameters
filename = strcat(fileprefix,'_z0.hdf');
fullpath = strcat(dest,filename);
ns = h5read(fullpath,'/SimulationParameters/npsets');    
nz = h5read(fullpath,'/SimulationParameters/nzones');    
dx = h5read(fullpath,'/SimulationParameters/zinfo/dxdydz');
nd = double(h5read(fullpath,'/SimulationParameters/nxnynzdomain'));
ts_info = h5info(fullpath,'/TimeStep');
nt = max(str2num(strrep(strcat(ts_info.Groups.Name),'/TimeStep/',' ')));
dt = h5read(fullpath,'/SimulationParameters/dt');

x = linspace(0,(double(nd(1))-1)*dx(1),double(nd(1)));
y = linspace(0,(double(nd(2))-1)*dx(2),double(nd(2)));
z = linspace(0,(double(nd(3))-1)*dx(3),double(nd(3)));
t = linspace(0,(double(nt)-1)*dt,double(nt));

region = zeros(nz,6);
region(1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');

% Read all other zone files for region information
for p=1:nz-1;
    filename = strcat(fileprefix,'_z',num2str(p),'.hdf');
    fullpath = strcat(dest,filename);
    region(p+1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');   
end

% Convert region data to processor cell limits
rcell = [region(:,1)/dx(1)+1 ...
         region(:,2)/dx(1)   ...
         region(:,3)/dx(2)+1 ...
         region(:,4)/dx(2)   ...
         region(:,5)/dx(3)+1 ...
         region(:,6)/dx(3)];

clear region
     
% Initialise field variables for full simulation domain
blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence));
if readB==true, 
    B = struct('x',blank,'y',blank,'z',blank);
end;
if readE==true, 
    E = struct('x',blank,'y',blank,'z',blank);
end;

% Initialise moment variables for full simulation domain
if readM==true,
    for p=0:ns-1
        % Find the name of each species for use in the particle moment
        % struct. For example: M.(pname).vx
        pname{p+1} = h5read(fullpath,strcat('/TimeStep/0/ParticleStats/PSet_',num2str(p),'/pname'));
    end
    for p=0:ns-1
        M.(pname{p+1}) =  struct('vx',blank,'vy',blank,'vz',blank,'n',blank);
    end
end
clear blank

dims = 'xyz';
momu = {'Vx' 'Vy' 'Vz' 'dn'};
moml = {'vx' 'vy' 'vz' 'n'};
% Read individual zone fields into full domain field variables

laststr = 0;
msg_old = 'Reading_000%\n';
for p=1:nz,
    for i=1:fcadence:(nt+1)
        % Display how far through the reading process we are
        msg = strcat('Reading_',num2str(100*(i+(p-1)*nt)/(nz*nt),'%3.3i'),'%%\n');
        % This next bit deletes the old message to save space in the command window
        if (~strcmp(msg,msg_old))
        fprintf(repmat('\b',1,laststr));
        fprintf(msg);
        laststr = numel(msg)-2;
        end
        msg = msg_old;
        
        filename = strcat(fileprefix,'_z',num2str(p-1),'.hdf');
        fullpath = strcat(dest,filename);
        
        % Read zone field into temporary variable, then copy into the
        % larger field, missing out ghost cells
        for k = 1:3;
            if readB==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/B',dims(k)));
                B.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/fcadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
            if readE==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/E',dims(k)));
                E.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/fcadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
        end % field loop
        if readM==true,
            % Do the same for the moments, looping over each particle species
            for s=0:ns-1
                for k=1:4
                    temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/Pset/',num2str(s),'/ZoneMoments/',momu{k}));
                    M.(pname{s+1}).(moml{k})(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/fcadence)) ...
                        = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
                end % moment loop
            end % species loop
        end
        
    end % timestep loop    
end % zone loop
clear temp