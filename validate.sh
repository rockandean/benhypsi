#!/bin/bash

# Check length of file                                       
FILELEN=`h5dump -y -d /TimeStep/200/ZoneFields/Bx $1_z0.hdf | tail -n+6 | tr -d '\t\n\r\f'| sed 's/,/\n/g' | wc -l`
SAMPLELEN="34943"
if [ "$FILELEN" = "$SAMPLELEN" ]; then
    echo "File is the same length as sample data"
else
    echo "File is not the same length as sample data - check initial conditions against config_bench, you may be using different processor geometry or domain size."
    exit 0
fi

# Extract data from hdf5 file into an ASCII file, chop off the header, remove white space
# then replace ',' delimiter with newline
# and take the first 1000 elements, before writing to a file for the upcoming comparison
h5dump -y -d /TimeStep/200/ZoneFields/Bx $1_z0.hdf | tail -n+6 | tr -d ' \t\n\r\f'| sed 's/,/\n/g' | head -n 1000 > data_clean.asci

# Check extracted data against test case data "sample_data.asci"
if diff data_clean.asci sample_data.asci; then
    echo "Data sets identical"
    echo "Code succefully reproduces test case - SUCCESS"
else
    echo "Data sets differ"
    echo "Code cannot reproduce test case - FAILED"
fi