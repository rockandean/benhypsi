# User path must contain bison, flex, kvf and hdf5 folders
# Compiler path can be checked using mpic++ file.cpp --showme:compile

USER_PATH=/home/space/phrlaz/benhypsi
COMPILER_PATH=/csc/minerva/software/gcc-4.7.3
HDF_PATH=/csc/minerva/software/gcc/4.7.3/HDF5/1.8.11-serial
#/gpfs/hpcwarwick/openmpi/1.6.4/gnu/4.6.1

################ Target objects are compiled here, objects at bottom  #########################

CPL=mpic++
CC=mpicc

INCLUDES=\
  -I$(USER_PATH)/kvf \
  -I$(HDF_PATH)/include \
  -I$(COMPILER_PATH)/include

DIAGFLAGS=\
#  -UDIAG_COLLECTNV\
#  -UDIAG_FIELD_L2\
#  -UDIAG_FIELD_DIVB\
#  -UDIAG_PMOVE\
#  -UDIAG_PTAF_L1\
#  -UDIAG_PTAF_BCF\
#  -UDIAG_PTTHR_L2\
#  -UDIAG_ZARRAY_L2\
#  -UDIAG_ZONE_L2



OPTIMIZE_OPTIONS=-O3
CPLFLAGS= $(OPTIMIZE_OPTIONS) $(INCLUDES) $(DIAGFLAGS)
CFLAGS=-v $(DIAGFLAGS) 

.cc.o:
	$(CPL) $(CPLFLAGS) -c $*.cc
.c.o:
	$(CC) $(CFLAGS) -c $*.c

################ End Target objects ######################
### Linking 

LIBDIRS=\
    -L$(USER_PATH)/kvf \
    -L$(HDF_PATH)/lib

LD_OPTIONS=\
    -Wl,-rpath -Wl,$(HDF_PATH)/lib

LIBS=-lhdf5 -lhdf5_hl -lkvf -lrt


# NOTE threading option included in load options taken from mpic++ 

MPI_LD_OPTIONS=\
-pthread -L$(COMPILER_PATH)/lib \
 -Wl,-rpath=$(COMPILER_PATH)/lib \
 -lmpi_cxx -lmpi -lopen-rte -lopen-pal -ldl -lm -lnuma -Wl,--export-dynamic -lrt -lnsl -lutil -lm -ldl
#-pthread -lmpi -lopen-rte -lopen-pal -ldl -Wl,--export-dynamic -lnsl -lutil



HYPSI_INCLUDES=\
  alfvenwic.h \
  discontic.h \
  exception.h \
  fams.h \
  hdf5adaptor.h \
  hypsi.h \
  output.h \
  ptcls.h \
  ptthr.h \
  rng.h \
  tags.h \
  timer.h \
  zonearray.h

HYPSI_MAIN_OBJ=hypsi_main.o

HYPSI_OBJS=\
  alfvenwic.o \
  collectnvt.o \
  discontic.o \
  exception.o \
  fams.o \
  hypsi.o \
  outputcontrol.o \
  ptcls.o \
  ptthr.o \
  rng.o   \
  ptaf.o \
  tags.o \
  pmove.o \
  uniformic.o \
  zonearray.o

all: hypsi_main

test_scalararray: test_scalararray.o zonearray.o exception.o ptcls.o rng.o
	$(CPL) -v -o test_scalararray\
           test_scalararray.o zonearray.o exception.o ptcls.o rng.o \
          $(LIBDIRS) $(LIBS) $(LD_OPTIONS) $(MPI_LD_OPTIONS)


hypsi_main: $(HYPSI_INCLUDES) $(HYPSI_MAIN_OBJ) $(HYPSI_OBJS)
	$(CPL)  -o hypsi_main $(HYPSI_MAIN_OBJ) $(HYPSI_OBJS) \
	$(LIBDIRS) $(LIBS) $(LD_OPTIONS) $(MPI_LD_OPTIONS)

MDOT_LIBDIR=-L/users/dhb/simulation/mdot/lib
MDOT_LIB=-lmdot
MDOT_INCLUDE=-I/users/dhb/simulation/mdot/include
bimaxtest: $(HYPSI_INCLUDES) bimaxtest.o $(HYPSI_OBJS)
	$(CPL) -o bimaxtest bimaxtest.o $(HYPSI_OBJS) $(MDOT_INCLUDE) $(LIBDIRS) $(LIBS)\
           $(MDOT_LIBDIR) $(MDOT_LIB)


clean:
	rm -f hypsi_main hypsi_main.o $(HYPSI_OBJS)    


bimaxtest.o: bimaxtest.cc
	$(CPL) -c bimaxtest.cc $(INCLUDES) $(MDOT_INCLUDE)


hypsi_main.o: hypsi.h
alfvenwic.o: 
collectnvt.o: hypsi.h
discontic.o: 
exception.o:
fams.o:
hypsi.o:
outputcontrol.o:
ptcls.o: ptcls.h zonearray.h
ptthr.o:
rng.o: rng.h
ptaf.o:
tags.o:
pmove.o:
uniformic.o: hypsi.h alfvenwic.h discontic.h exception.h rng.h ptthr.h timer.h\
             zonearray.h ptcls.h fams.h tags.h
zonearray.o: zonearray.h

