#include <iostream>

#include "hypsi.h"
#include "output.h"
#include "hdf5adaptor.h"
#include "alfvenwic.h"
#include "discontic.h"

#ifdef DIAG_BLOBS3D
#include "blobs3d.h"
#endif

using namespace std;
using namespace HYPSI;


main( int argc, char** argv )
{
  Timer timer;

  Timer ptcle_timer;

  try
  {
    
  Hypsim        hypsim;
  UniformIC     uniform_initial_conditions;
#ifdef DIAG_BLOBS3D
  Blobs3dIC       blobs3d_initial_conditions;
#endif
  AlfvenWaveIC  alfvenwave_initial_conditions;
  DiscontIC     discontinuity_initial_conditions;

  hypsim.mpi_comm.initialize( &argc, &argv );
  /*   cout << "NODE: " << hypsim.mpi_comm.thisnode 
       << " / "<< hypsim.mpi_comm.nnodes << "\n"; */

  hypsim.initialize( argc, argv );
  
  string zone_str;
  { stringstream sstr;
    sstr.str(""); sstr<<"Z"<< hypsim.mpi_comm.thisnode; zone_str = sstr.str();
  }
  //  cout << "Z" << hypsim.mpi_comm.thisnode << ": Done hypsim.initialize \n";
  

  if( hypsim.mpi_comm.thisnode==0 )
    hypsim.zinfo.print( cout, "Zone " );
  
// -----------------------------    Initialize particle boundary conditions
// -----------------------------    Initialization for given PLASMA initial conditions  

  hypsim.initialize_periodic_bc(); //! particle_bcf = new PeriodicBCF( sim_params.domain_region );
  //cout << "Z" << hypsim.mpi_comm.thisnode << ": Done initialize_periodic_bc \n";

  string initial_conditions_str = hypsim.sim_params.initial_condition_str;

  
  if     ( initial_conditions_str == "UniformPlasma" )
  {

    uniform_initial_conditions.read_input_data( hypsim.config_data );
    uniform_initial_conditions.initialize_Bfield( hypsim );
    hypsim.initialize_uniform_resis( hypsim.sim_params.uniform_resis );
    hypsim.initialize_pe( hypsim.sim_params.initial_Te );
    timer.start();
    uniform_initial_conditions.initialize_particle_sets( hypsim );
    timer.stop();
    //    timer.print( cout, zone_str+": initialize_particle_sets " );
  }
#ifdef DIAG_BLOBS3D
  else if( initial_conditions_str == "Blobs3D"       )
  { 

    blobs3d_initial_conditions.read_input_data( hypsim.config_data );
    blobs3d_initial_conditions.initialize_uniform_params( hypsim );
    blobs3d_initial_conditions.pmove_fields_and_particles( hypsim );

   } 
#endif
  else if( initial_conditions_str == "Discontinuity" )
  {

    discontinuity_initial_conditions.read_input_data( hypsim.config_data );
    discontinuity_initial_conditions.initialize_uniform_params( hypsim );
    discontinuity_initial_conditions.initialize_fields_and_particles( hypsim );
    //if( hypsim.mpi_comm.thisnode == 0 )
      //discontinuity_initial_conditions.output_params(cout, zone_str+": " );
  } 
  
  else if( initial_conditions_str == "AlfvenWave"    )
  {
    alfvenwave_initial_conditions.read_input_data( hypsim.config_data );
    alfvenwave_initial_conditions.initialize_fields_and_particles( hypsim );
    //if( hypsim.mpi_comm.thisnode == 0 )
       //alfvenwave_initial_conditions.output_params(cout, zone_str+": " );

  }

  // special for divergence cleaning (TO BE BACKED-OUT) ----------------
  hypsim.zfields->set_psi_uniform( 0.0 );
  hypsim.zhfields->set_psi_uniform( 0.0 );
   

  if( hypsim.mpi_comm.thisnode==0 )
    for( int i=0; i<hypsim.sim_params.npsets; ++i )
      hypsim.psets[i]->print(cout, zone_str+": " );


// --------------------------------------------------------- Initial output

  hypsim.ts_output_control.initialize_from_config_data( hypsim.config_data );
  hypsim.pdata_output_control.initialize_from_config_data( hypsim.config_data );

  OutputManager outputmgr;
  HypsiOutputAgent<HDF5OutputAdaptor> outputagent( &hypsim );
  {
    stringstream sstr;
    sstr << hypsim.sim_params.hostzone;
    string outputfilename = hypsim.sim_params.output_datafile_root +"_z"+ sstr.str()+".hdf";
    outputagent.open( outputfilename );

  }

  outputmgr.push_back( &outputagent );
  outputmgr.output( "SimulationParameters", 0 );

// -------------------------------------------------------------- start up

  for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset )
  {
    //cout<< "Z" << hypsim.mpi_comm.thisnode<<": SORTING .. Particle set: " << ipset << "\n";
    hypsim.psets[ipset]->sort_by_zone( hypsim.domain_info, hypsim.zones_info );
    // cout<< "Z" << hypsim.mpi_comm.thisnode<<": ACTIVE ZONES: "
    //<< hypsim.psets[ipset]->n_active_zones << "\n";

  }

  hypsim.pmove( 0.0 );
  MPI_Barrier( MPI_COMM_WORLD );

  if( hypsim.mpi_comm.thisnode==0 )
    cout<< "Z" << hypsim.mpi_comm.thisnode<<": COMPLETED zero ts PMOVE: \n";
  
  for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset )
  {
    //cout<< "Z" << hypsim.mpi_comm.thisnode<<"MAKE PERIODIC ipset: " << ipset << "\n";
    hypsim.pset_pzmoms[ipset]->make_xyzperiodic();

  }

  hypsim.calc_nvq_moments();
  //cout<< "Z" << hypsim.mpi_comm.thisnode<<"COMPLETED calc_nvq_moments:\n";
  
  hypsim.calc_pe_adiabatic_xyzperiodic(*(hypsim.pe),
                                       *(hypsim.ep_qmoms),
                                       hypsim.sim_params.initial_Te,
                                       hypsim.sim_params.electron_gamma );

  hypsim.zfields->calcE( *(hypsim.ep_qmoms), *(hypsim.pe), *(hypsim.resis) );
  hypsim.zfields->make_E_xyzperiodic();
  hypsim.zhfields->copy_EBdata( *(hypsim.zfields) );

// ------------------------------------------------- timestep zero output

  // cout<< "Z" << hypsim.mpi_comm.thisnode
  // <<"Done initialization - about to output for timestep zero:\n";

  MPI_Barrier( MPI_COMM_WORLD );
  hypsim.allocate_pset_nvtmoms();
  hypsim.collect_nvtmoms_xyzperiodic( true ); // collectT bool - was false
  hypsim.ts_output_control.output( outputmgr, hypsim.ts );
  hypsim.free_pset_nvtmoms();
  hypsim.pdata_output_control.output( outputmgr, hypsim.ts );

  // cout<< "Z" << hypsim.mpi_comm.thisnode
  //    <<"Completed output for timestep zero:\n";
  MPI_Barrier( MPI_COMM_WORLD );

// ----------------------------------------------------- end start up

  if( hypsim.mpi_comm.thisnode==0 )
    cout<<"Z"<< hypsim.mpi_comm.thisnode<<" About to run pmove for: "
    << hypsim.sim_params.ntimesteps << " timesteps\n";


// ====================================================-=== START OF MAIN LOOP
  for( int i=0; i < hypsim.sim_params.ntimesteps; ++i )
  {

    //  cout << "Z" << hypsim.mpi_comm.thisnode << ": STEP: " << i << "\n";
  
  timer.start();

  hypsim.calc_pe_adiabatic_xyzperiodic(
            *(hypsim.pe),
            *(hypsim.mp_qmoms),
            hypsim.sim_params.initial_Te,
            hypsim.sim_params.electron_gamma );

  //cout<<"Z"<<hypsim.mpi_comm.thisnode << ": done calc_pe_adiabatic_xyzperiodic\n";

  hypsim.advanceEB_xyzperiodic( *(hypsim.zfields), *(hypsim.zhfields),
                        0.5*hypsim.sim_params.dt,
                        hypsim.sim_params.nsubsteps,
                        *(hypsim.mp_qmoms),
                        *(hypsim.pe), *(hypsim.resis),
                        hypsim.sim_params.smoothfac_EinEBadvance );

  //cout<<"Z"<<hypsim.mpi_comm.thisnode << ": done advanceEB_xyzperiodic\n";

  hypsim.zfields->average_with_EBdata( *(hypsim.zhfields) );

  //cout<<"Z"<<hypsim.mpi_comm.thisnode << ": done average_with_EBdata\n";

  hypsim.zhfields->copy_EBdata( *(hypsim.zfields) );

  //cout<<"Z"<<hypsim.mpi_comm.thisnode << ": done copy_EBdata\n";
  

  hypsim.calc_camE_xyzperiodic( 0.5*hypsim.sim_params.dt, *(hypsim.zfields),
                       *(hypsim.mp_qmoms),
                       *(hypsim.ep_qmoms),
                       *(hypsim.ca_qmoms),
                       *(hypsim.pe), *(hypsim.resis) );

  //cout<<"Z"<<hypsim.mpi_comm.thisnode << ": done calc_camE_xyzperiodic\n";

  for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset )
  {
    /*
    cout<< "Z" << hypsim.mpi_comm.thisnode<<": SORTING .. Particle set: " << ipset << "\n";
    */
    hypsim.psets[ipset]->sort_by_zone( hypsim.domain_info, hypsim.zones_info );


    /*
    if( hypsim.mpi_comm.thisnode < 8 )
      cout<< "Z" << hypsim.mpi_comm.thisnode<<": ACTIVE ZONES: "
        << hypsim.psets[ipset]->n_active_zones << "\n";
    */
  }

  ptcle_timer.start();

  hypsim.pmove( hypsim.sim_params.dt );

  ptcle_timer.stop();

/*
if( hypsim.mpi_comm.thisnode < 16 )
  ptcle_timer.print( cout, zone_str+": PMOVE: " );
*/

  //  cout<< "Z" << hypsim.mpi_comm.thisnode<<": COMPLETED PMOVE: \n";

// make pset contributions to moments periodic

  for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset )
  {

    //    cout<< "Z" << hypsim.mpi_comm.thisnode<<": MAKE PERIODIC ipset: " << ipset << "\n";

    hypsim.pset_pzmoms[ipset]->make_xyzperiodic();

  }

  hypsim.calc_nvq_moments();

  //  cout<< "Z" << hypsim.mpi_comm.thisnode<<" COMPLETED calc_nvq_moments:\n";
  
  hypsim.calc_pe_adiabatic_xyzperiodic(
            *(hypsim.pe),
            *(hypsim.mp_qmoms),
            hypsim.sim_params.initial_Te,
            hypsim.sim_params.electron_gamma );

  hypsim.advanceEB_xyzperiodic( *(hypsim.zfields), *(hypsim.zhfields),
                        0.5*hypsim.sim_params.dt, hypsim.sim_params.nsubsteps,
                        *(hypsim.mp_qmoms),
                        *(hypsim.pe), *(hypsim.resis),
                        hypsim.sim_params.smoothfac_EinEBadvance );

  hypsim.zfields->average_with_EBdata( *(hypsim.zhfields) );
  hypsim.zhfields->copy_EBdata( *(hypsim.zfields) );

// increment timestep counter;

  hypsim.ts++;

  timer.stop();

  if( hypsim.mpi_comm.thisnode == 0 )
  {
    stringstream s; s<<hypsim.ts;
    timer.print( cout, zone_str + ": TS: "+s.str()+" " );
  }

  //  cout<<"Z"<<hypsim.mpi_comm.thisnode<<": ABOUT TO perform TIMESTEP OUTPUT\n";

  bool nvcollection_this_ts;
  
  nvcollection_this_ts =
    hypsim.ts_output_control.test("NVmoments",hypsim.ts);

  if( nvcollection_this_ts )
  {
    hypsim.allocate_pset_nvtmoms();
  
    for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset )
    {
      //      cout<< "Z" << hypsim.mpi_comm.thisnode
      //          <<": Sorting for NVmoments .. Particle set: " << ipset << "\n";

      hypsim.psets[ipset]->sort_by_zone( hypsim.domain_info, hypsim.zones_info );
    }

    hypsim.collect_nvtmoms_xyzperiodic( true ); // collectT bool - was false
  }

// Timestep output
  hypsim.ts_output_control.output( outputmgr, hypsim.ts );
  
  if( nvcollection_this_ts )
    hypsim.free_pset_nvtmoms();

// particle output

  //cout << "test for mass" << hypsim->UniformIC->_psmass[1] << "\n";

  hypsim.pdata_output_control.output( outputmgr, hypsim.ts );

  outputagent.flush();

  } // end of step loop

  if( hypsim.mpi_comm.thisnode==0 )
    cout <<zone_str<< ": COMPLETED: " << hypsim.sim_params.ntimesteps
         << " timesteps\n";

  MPI_Barrier( MPI_COMM_WORLD );

// let processes complete any any output ...

  if( hypsim.mpi_comm.thisnode==0 )
  cout << "Z" << hypsim.mpi_comm.thisnode
       << " ABOUT TO CLOSE OUTPUT FILE (AGENT)\n";

  outputagent.close();

  if( hypsim.mpi_comm.thisnode==0 )
  cout << "Z" << hypsim.mpi_comm.thisnode
       << " MPI_Barrier after closing output file\n";

  MPI_Barrier( MPI_COMM_WORLD );

  sleep(1);

  } catch( HypsiException& e )
  {
    cout << "HYPSI Exception at main level\n";
    e.diag_cout();
    cout << endl;
  }

  sleep(1);

}
