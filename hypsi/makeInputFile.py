#!/usr/bin/python2.6
# line 136 has Xint
# line 125 timesteps
# line 129 geometry
# line 361 drift vel
"""test isFile, delete/create new; open dic object fileDic"""

import os
import numpy as np

fname="input_file"
fileDic={}
try:
    if (os.path.isfile(fname)):
        os.remove(fname)
        fileDic["name"] = fname
        fnameOpen=open(fname, 'w')
    else:
        fileDic["name"] = fname
        fnameOpen=open(fname, 'w')
except:
    print "Problems with input_file I/O \n"
    pass

"""**************************************
read System Simulation Parameters
**************************************"""
"""SI"""

Blob    = True
Ice     = False
Uniform = False
ICstr   = '"UniformPlasma"'     #'"Blobs3D"' '"UniformPlasma"'

os.system("echo Blob = "+str(Blob))
os.system("echo running = "+str(ICstr))
""" Blob """
if Blob:
    B               =   0.33           #tesla
    n_background    =   8.1e20         # m^-3
    # T               =   4e6             # Kelvin
    T               = 344.75*1.6022e-19/1.3807e-23 # eV to K 
    #n_blob          =   5               # (9+1)* n_background m^-3 becuase using as two species
    #blob_radius     =   5/np.sqrt(2*np.log(2)) #FWHM=2*sqrt(2*ln(2))*sigma=10*gyroradius
    cellSize        =   0.1 # gyroRadius
    n_1             =   1.0 # weighting of particles
    n_2             =   1.0

""" Ice """
if Ice:
    B               =   2.1             # tesla
    T               =   1.1605e7        # Kelvin
    n_background    =   1e19            # m^-3
    #
    n_1             =   0.99800399201   # species density:deuteron
    n_2             =   9.9800399e-4    # species density:alpha
    cellSize        =   0.0302179       # ion skindepth
    cellSize_a      =   cellSize        # use to write in file

""" Uniform"""
if Uniform:
    B               =   0.4             #tesla
    n_background    =   1e19            # m^-3
    T               =   4e6             # Kelvin
    #
    #n_blob          =   5               # (9+1)* n_background m^-3 becuase using as two species
    gyroRadius      =   8e-3            # m
    cellSize        =   0.4  # gyroRadius

""" *** Units *** """

def Va(_Bfield, _n):
    """Alfven Speed"""
    permeability    =   4*np.pi*1e-7    # H m^-1
    mass_proton     =   1.6726e-27      # kg
    return _Bfield/np.sqrt(permeability*_n*mass_proton)
def Va2(_Bfield, _n):
    """Alfven Speed square"""
    permeability    =   4*np.pi*1e-7    # H m^-1
    mass_proton     =   1.6726e-27      # kg
    return _Bfield*_Bfield/(permeability*_n*mass_proton)


permeability    =   4*np.pi*1e-7    # H m^-1   H=kg m^2/C^2 =T m^2/A
kB              =   1.3807e-23      # J / K^-1
q               =   1.6022e-19      #  Coulomb
mass_proton     =   1.6726e-27      # kg
alfvenSpeed     =   Va(B, n_background)
print "alfvenSpeed:", alfvenSpeed
alfvenSpeed2    =   Va2(B, n_background)     
omega_i         =   q*B/mass_proton                    # ion gyrofrequency rad/seg
gyroRadius      =   np.sqrt(2*mass_proton*kB*T)/(B*q)  # m  T ion or e?
print "Gyro-radius:", gyroRadius
T_unit          =   alfvenSpeed2*mass_proton/kB        # Kelvin
L_unit          =   alfvenSpeed/omega_i              # ion skin depth

if Blob: 
    ratio_unitGyro  =   gyroRadius/L_unit
    print "ratio_unit:", ratio_unitGyro

T_e             =   str(T/T_unit)   # dimensionless
print "T_e:       ",T_e

""" ****** *** domain/allocPart/time  ****** *** """
def truncar(_A):
    if Blob: err=1e4
    if Ice: err =1e7
    return np.round(_A*err)/err
"""                                      """

if Ice:
    timeSteps       =   100000
    geometry        =   [64, 1, 1]
    sim_domain      =   [2048, 1, 1]    # cells and units on length
    pPerCell        =   100             # same each species
    init_one        =   np.prod(sim_domain)/np.prod(geometry)*pPerCell  # same each species
    initParticles   =   str(init_one)+", "+str(init_one)
    allocParticles  =   str(int(init_one)+int(init_one)*10/100)+", "+str(int(init_one)+int(init_one)*10/100)
    lXyz0 		   =   truncar(sim_domain[0]*cellSize)
    lXyz1 		   = 	 truncar(sim_domain[1]*cellSize)
    lXyz2 		   = 	 truncar(sim_domain[2]*cellSize)
    lXyz            =   [ lXyz0 , lXyz1 , lXyz2 ]
    lXyz_str        =   str(lXyz0) + ", "+ str(lXyz1) +", "+ str(lXyz2)


if Blob:
    timeSteps       =   50000
    fieldValue      =   1.6
    EBCadence       =   [50,50,1000]
    dt 		        = 	0.001
    geometry        =   [16, 4, 4] # 9,4,4 144:   16,4,4 256:   
    sim_domain      =   [400, 100, 100]# 16*50=800 16*64=1024 6*170=1020 6*135=810 # cells and units on length
    pPerCell        =   100
    init_one        =   np.prod(sim_domain)/np.prod(geometry)*pPerCell
    cellSize_a      =   truncar(cellSize*ratio_unitGyro)     # use to write in file
    blob_radius     =   15          # gyroRadius, used as 10*gyroRadius,  cellsize*blob_radius
#
    Xnodes          = geometry[1]*geometry[2]#  256:4x4 36:3x3 
    Xnparticles     = pPerCell
    Xint            = 76 # 60,  105
    XnumberOfParticles =  int(Xnparticles*Xint)# 100*6, 100*0.85, 2130 one core
    Xcellsize       = cellSize_a
    Xgrid           = sim_domain[2]
    Xratioskindepth = ratio_unitGyro
    Xblob_radius    =   8.2 # blob_radius*0.546
    XN = Xnodes * XnumberOfParticles # is per zone
    Xvther=[Xcellsize*Xgrid/2, Xcellsize*Xgrid/2, Xcellsize*Xblob_radius]
    print "box size:  ", Xcellsize*Xgrid, Xcellsize*Xgrid
    print "FWHM",2*np.sqrt(2*np.log(2))*Xvther[2]/Xratioskindepth, "Larmor radius units"
    print "FWHM 2D",2*np.sqrt(np.log(2))*Xvther[2]/Xratioskindepth, "Larmor radius units"
    Xx = np.random.normal(Xvther[0], Xvther[2], XN)
    Xy = np.random.normal(Xvther[0], Xvther[2], XN)
    XbinWidth= Xcellsize;
    Xbins = np.arange(Xvther[1]-3*Xvther[2], Xvther[1]+3*Xvther[2] + XbinWidth, XbinWidth);
    XH, xedges, yedges = np.histogram2d(Xy, Xx, bins=(Xbins, Xbins))
    print "n_peak::",np.max(XH), "minx: ",np.min(xedges)
#
    N_peakPerCell   =   pPerCell*Xint*sim_domain[0]/geometry[0] # 850
    init_two        =   N_peakPerCell
    #
    initParticles   =   str(init_one)+", "+str(init_two)
    allocParticles  =   str(int(init_one)+int(init_one)*10/100)+", "+str(int(init_two)+int(init_two)*10/100)
    #
    #cellSize_a      =   truncar(cellSize*ratio_unitGyro)                    # use to write in file
    #blob_radius     =   15          # gyroRadius, used as 10*gyroRadius,  cellsize*blob_radius
    blob_radius_a   =   str(truncar(blob_radius*cellSize_a)) # vther=blob_radius*sqrt(2), 2*sigma^2==vther^2
    lXyz0           =   truncar(sim_domain[0]*cellSize_a)
    lXyz1           =   truncar(sim_domain[1]*cellSize_a)
    lXyz2           =   truncar(sim_domain[2]*cellSize_a)
    lXyz_str        =   str(lXyz0)+", "+str(lXyz1)+", "+str(lXyz2)
    lXyz            =   [lXyz0, lXyz1, lXyz2]
    #
    xcentre         =   str(truncar(lXyz1/2))
    ycentre         =   str(truncar(lXyz2/2))
    vthermal 		= 	np.sqrt(kB*T/mass_proton)/alfvenSpeed

if Uniform:
    geometry        =   [1, 1, 1]
    sim_domain      =   [1024, 1, 1]# 16*50=800 16*64=1024 6*170=1020 6*135=810 # cells and units on length
    timeSteps       =   100000
    dt              =   0.002
    pPerCell        =   25
    init_one        =   np.prod(sim_domain)/np.prod(geometry)*pPerCell
    N_peakPerCell   =   850
    init_two        =   N_peakPerCell
    #
    initParticles   =   str(init_one)+", "+str(init_two)
    allocParticles  =   str(int(init_one)+int(init_one)*10/100)+", "+str(int(init_two)+int(init_two)*10/100)
    #
    cellSize_a      =   truncar(cellSize*ratio_unitGyro)                    # use to write in file
    lXyz0           =   truncar(sim_domain[0]*cellSize_a)
    lXyz1           =   truncar(sim_domain[1]*cellSize_a)
    lXyz2           =   truncar(sim_domain[2]*cellSize_a)
    lXyz_str        =   str(lXyz0)+", "+str(lXyz1)+", "+str(lXyz2)
    lXyz            =   [lXyz0, lXyz1, lXyz2]
    #
    xcentre         =   str(truncar(lXyz1/2))
    ycentre         =   str(truncar(lXyz2/2))
    vthermal 	   =   np.sqrt(kB*T/mass_proton)/alfvenSpeed

 

"""*********************************************************
Input values are passed here from the above physical system
*********************************************************"""
"""
// INPUT FILE FOR HYPSI

//    - SIMULATIONS PARAMETERS
//    - OUTPUT CONTROL * TIME STEP * PARTICLE DATA
//    - PARTICLE SETS (Protons, alpha, etc.)
//    - UNIFORM PLASMA INITIAL CONDITIONS
//    - ALFVEN WAVE INITIAL CONDITIONS

// SIMULATIONS PARAMETERS --------------------------------------------
//  - Output Data File name. Actual file name is <root_part>_z<n>.hdf
//  - time_step   :: Time step dt in Omega_{cp}^{-1}
//  - n_timesteps :: Number of time steps for simulation
//  - Number of substeps for field solution
//  - Value of uniform resistivity
//  - Initial electron temperature T_e
//  - Gamma (ratio of specific heats) for adiabatic electron fluid:
//           gamma = 1  for isothermal
//           usual value is 5/3
//  - COMPUTE NODE GEOMETRY  format: { nx_cn, ny_cn, nz_cn }
//            product must equal number of nodes in MPI world
//            number of cells in domain must be commensurate with number of
//            compute nodes in each xyz direction
//  - size of full simulation domain (ion inertial lengths) { x, y, z }
//  - cellsize (ion inertial lengths) { dx, dy, dz }
//  - Seed for particle random number generator - use a negative integer!
//            particle_rng_seed = -93821;
//  - SMOOTHING CONTROL, smoothing factors between 0 (no smoothing) and 1.0
"""
Smooth                      =       0.3
nodes_xyz_geometry          =       "{ "+str(geometry)[1:-1]+"}"
domain_LxLyLz               =       "{ "+lXyz_str+" }" 
cell_dxdydz                 =       "{ "+str(cellSize_a)+", "+str(cellSize_a)+", "+str(cellSize_a)+" }"
time_step                   =       str(dt)
n_timesteps                 =       str(timeSteps)
ouputFile_rootName          =       '"data_'+ICstr[1:]#+'_'+''.join(str(Smooth).split('.'))+'"' 
n_field_substeps            =       "4"
electron_temperature        =       T_e
electron_fluid_gamma        =       "1.0000000000000"
uniform_resistivity         =       "0.000000000"
rng_seed                    =       "-17"
smoothfac_EinEBadvance      =       str(Smooth)
initial_condition_str       =       ICstr

os.system("echo "+initial_condition_str)
#names can be: UniformPlasma, Blobs3D, Discontinuity, AlfvenWave
""" Pass first set of tags """

fileDic["simParams"] =[
"compute_nodes_xyz_geometry = " + nodes_xyz_geometry + ";",
"domain/xyzsize = " + domain_LxLyLz + ";",
"domain/xyzcell = " + cell_dxdydz + ";",
"initial_condition_str = " + initial_condition_str + ";",
"time_step = " + time_step + ";",
"n_timesteps = " + n_timesteps + ";",
"output_control/data_filename_root = " + ouputFile_rootName + ";",
"n_field_substeps = " + n_field_substeps + ";",
"uniform_resistivity = " + uniform_resistivity + ";",
"initial_electron_temperature = " + electron_temperature + ";",
"electron_fluid_gamma = " + electron_fluid_gamma + ";",
"particle_rng_seed = " + rng_seed + ";",
"smoothing_control/smoothfac_EinEBadvance = " + smoothfac_EinEBadvance + ";"
]

"""
// OUTPUT CONTROL -------------------------------------------------------
//  - TIME STEP: Each control item has: tag, start, end, cadence
//            The number of items in start, end, cadence must be AT LEAST
//            the number of tags. The tag names are used for output selection,
//            and should match what is used in the code.
//
//  - PARTICLE DATA: Defines a set of samples: each will output particle data for
//           (i) a given list of particle sets { 0,1, etc. }
//           (ii) every given nth particle (particle cadence)
//           (iii) in a given xyz region {x0,x1,y0,y1,z0,z1}
//           (iv) at a given set of timesteps {start, end, cadence}
"""
tagStart="0,"
tagEnd= str(timeSteps)+","
tagCadence=''.join(str(EBCadence).strip(']').strip('['))
#output_tags                 =       '{"Bfield", "Efield" ,"Tmoments" ,"NVmoments" }'
#ts_start                    =       "{ "+tagStart+tagStart+tagStart+tagStart[:-1]+" }"
#ts_end                      =       "{ "+tagEnd+tagEnd+tagEnd+tagEnd[:-1]+" }"
#ts_cadence                  =       "{ "+tagCandence+tagCandence+tagCandence+tagCandence[:-1]+"}"

output_tags                 =       '{"Bfield", "Efield" ,"NVmoments" }'
ts_start                    =       "{ "+tagStart+tagStart+tagStart[:-1]+" }"
ts_end                      =       "{ "+tagEnd+tagEnd+tagEnd[:-1]+" }"
ts_cadence                  =       "{ "+tagCadence+"}"

pdata_num_samples           =       " 0 "
pdata_sample_0_pset_list    =       "{ 0 , 1}"
pdata_sample_0_nth_Pcadence =       "1"
pdata_sample_0_region       =       "{ 0, 50, 0, 50, 0, 50 }"
pdata_sample_0_ts_control   =       "{ 0, 100000, 10 }"
os.system("echo data samples"+ pdata_num_samples)
""" Pass second set of tags """

fileDic["output"]=[
"ts_output_control/tags = " + output_tags + ";",
"ts_output_control/ts_start = " + ts_start + ";",
"ts_output_control/end_ts   = " + ts_end + ";",
"ts_output_control/ts_cadence = " + ts_cadence + ";",
"pdata_output_control/num_samples = " + pdata_num_samples + ";",
"pdata_output_control/sample_0/pset_list = " + pdata_sample_0_pset_list + ";",
"pdata_output_control/sample_0/nth_particle_cadence = " + pdata_sample_0_nth_Pcadence + ";",
"pdata_output_control/sample_0/region = " + pdata_sample_0_region + ";",
"pdata_output_control/sample_0/ts_control = " + pdata_sample_0_ts_control + ";"
]

"""
// UNIFORM PLASMA INITIAL CONDITIONS -------------------------------------
// PARTICLE SETS ---------------------------------------------------------
//  - Number of particle sets
//          Following entries should be array with
//          AT LEAST num_sets elements
//  - Initial allocation of space for particles PER NODE
//           NB: This is on a PER NODE basis
//           (cf particle_sets/initial_total_active_number)
//  - Initial number of active particles per node or MPI process
//           NB: the number on each MPI node is this number. An exact
//           integer decrease extra particles in region, see UniformIC
//  - Name to be used to identify particle set
//  - Particle mass in m_p (proton mass)
//  - Particle charge in q_p (proton charge)
//  - Test particle flags
//          1 .. treat particle set as test particles
//          0 (or != 1) .. include particle set in moment calculations
// UNIFORM_IC -------------------------------------------------------------
//  - Initial number density for each particle set
//            NB: This, together with initial_number, sets the statistical
//            weighting factor for each particle set.
//  - Initial parallel thermal velocity (shifted Bimaxwellian initialization)
//  - Initial perpendicular thermal velocity (shifted Bimaxwellian initialization)
//  - Initial Thermal velocity ( //, perp  not used) ( Maxwellian Initialization)
//  - Initial parallel shift velocity (shifted Bimaxwellian initialization)
//  - Initial xyz shift velocity (shifted Bimaxwellian initialization)
//            each particle set requires x,y,z elements of shift vector
//  - Initial magnetic field
"""
if Blob:
    num_psets               =       " 2 "
if Blob and ICstr=='"UniformPlasma"':
    num_psets               =       " 1 "
allocation_per_node         =       "{"+allocParticles+" }"
active_per_node             =       "{"+initParticles+" }"
pset_name                   =       '{ "protons", "blob" }'
mass_mp                     =       "{ 1, 1 }"
charge_qp                   =       "{ 1, 1 }"
test_particle_flag          =       "{ 0, 0 }"

IC_number_density           =       "{ "+str(n_1)+", "+ str( n_2)+" }"
IC_vth_par                  =       "{ 0.029, 0.03 }"
IC_vth_perp                 =       "{ 0.059, 0.03 }"
IC_vthermal                 =       "{" + str(vthermal) + "}" # not working
IC_Vpar_shift               =       "{ 0.0, 0.0 }"
IC_Vxyz_shift               =       "{ 0.0, 0.0, 0.0, 0.0, 0.2, 0.0 }"
IC_Bxyz_vec                 =       "{ "+str(fieldValue)+", 0.0, 0.0 }"

""" Blob centre position in domain """
if Blob:
    blob_position_centre        =       "{" +xcentre+", "+ ycentre+", "+blob_radius_a+"}" # deleted after Blob3d.cc is ready
""" Pass third set of tags  """

fileDic["particles"] = [
"particle_sets/num_psets = " + num_psets + ";",
"particle_sets/initial_allocation_per_node = "+ allocation_per_node + ";",
"particle_sets/initial_active_per_node = "+ active_per_node + ";",
"particle_sets/pset_name = "+pset_name + ";",
"particle_sets/mass_mp = "+ mass_mp + ";",
"particle_sets/charge_qp = "+ charge_qp + ";",
"particle_sets/test_particle_flag = "+ test_particle_flag + ";",
"uniformIC/number_density = "+ IC_number_density + ";",
"uniformIC/vth_par = "+ IC_vth_par  + ";",
"uniformIC/vth_perp = "+IC_vth_perp    + ";",
"uniformIC/vthermal = "+ IC_vthermal + ";",
"uniformIC/Vpar_shift = "+ IC_Vpar_shift  + ";",
"uniformIC/Vxyz_shift = "+ IC_Vxyz_shift  + ";",
"uniformIC/Bxyz_vec = "+ IC_Bxyz_vec + ";",
"uniformIC/position_centre = " + blob_position_centre + ";"# deleted after Blob3d.cc is ready
]

""" Pass selection tags:blob, AlfvenWaves etc """

"""//  BLOB3D PLASMA INITIAL CONDITIONS -----------------------------------"""
if Blob:
    blob_center                 =       "{" +xcentre+ ", "+ ycentre+"}"
    blob_width                  =       blob_radius_a

    fileDic["selectionTag"] = [
    "blobs3d/blob_center = " + blob_center + ";",
    "blobs3d/blob_width  = " +blob_width +";"
    ]

"""
// ALFVEN WAVE INITIAL CONDITIONS -------------------------------------
//  - Mode numbers mx, my, mz, supplied as array of three integers
//  - Propagation direction:
//      +1 for parallel to B (defined by mode numbers)
//      -1 for anti-parallel to B
//  - Complex amplitudes for LH (spatial) and RH (spatial)  B  waves

//# alfven_wave_initial_conditions/mode_numbers = { 3, 0, 0 };
//# alfven_wave_initial_conditions/propagation_direction = -1;
//# alfven_wave_initial_conditions/aplus_cmplxamp = { 0.0, 0.0 };
//# alfven_wave_initial_conditions/aminus_cmplxamp = { 0.0, 0.0 };
// -----------------------------------------------------------------------
#// Initial xyz shift velocity (shifted Bimaxwellian initialization)
#// each particle set requires x,y,z elements of shift vector
"""

"""
#0.- Missing a few keys from updated input_file
#1.- Calculate new values for system
#2.- set tags for selectin tag
#3.- update values inside fileDic
 """

""" Write to input_file """
if initial_condition_str=='"UniformPlasma"':
    tagsList=["simParams","output","particles"]
else:
    tagsList=["simParams","output","particles","selectionTag"]

for line in tagsList:
    for i in range(np.size(fileDic[line])):
        fnameOpen.write(fileDic[line][i]+"\n")

fnameOpen.close()


