#!/bin/bash
numargs=$#
if [ $# -ne 3 ]
then
 echo "Not enough arguments - must have 3D array"
 exit
fi

TOTPROC=$(((( $1 * $2 )) * $3 ))
echo  "Total processors: $TOTPROC"

PROC_PREFIX=("compute_nodes_xyz_geometry = ")
FORM_PROC_OLD=`grep "compute_nodes_xyz_geometry" config_bench | sed "s/compute_nodes_xyz_geometry = //g" | sed "s/;//g"`
echo "Current configuration: $FORM_PROC_OLD"

FORM_PROC_NEW="{ $1, $2, $3 }"
echo "Re-writing config file with zone configuration: $FORM_PROC_NEW"
sed -i "s/$PROC_PREFIX$FORM_PROC_OLD/$PROC_PREFIX$FORM_PROC_NEW/g" config_bench

# Set the particles-per-processes such that there are 100 particles per cell
PARTS_PREFIX=("initial_active_per_node = ")
FORM_PARTS_OLD=`grep "particle_sets\/initial_active_per_node = " config_bench | sed "s/particle_sets\/initial_active_per_node = //g"`
echo "Old file's particle-per-process:"
echo $PARTS_PREFIX$FORM_PARTS_OLD
PART_TOT=21600000
echo "Re-writing config file with:"
FORM_PARTS_NEW="{ $(( $PART_TOT / $TOTPROC )) };"
echo $PARTS_PREFIX$FORM_PARTS_NEW
sed -i "s/$PARTS_PREFIX$FORM_PARTS_OLD/$PARTS_PREFIX$FORM_PARTS_NEW/g" config_bench

# Set the total particle allocation to 110% of the particles-per-process
ALLOC_PREFIX=("initial_allocation_per_node = ")
FORM_ALLOC_OLD=`grep "particle_sets\/initial_allocation_per_node = " config_bench | sed "s/particle_sets\/initial_allocation_per_node = //g"`
FORM_ALLOC_NEW="{ $(((( 11 * (( $PART_TOT / $TOTPROC )) )) / 10 )) };"
echo $ALLOC_PREFIX$FORM_ALLOC_NEW
sed -i "s/$ALLOC_PREFIX$FORM_ALLOC_OLD/$ALLOC_PREFIX$FORM_ALLOC_NEW/g" config_bench
