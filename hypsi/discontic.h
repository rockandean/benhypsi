#ifndef _HYPSI_DISCONTIC_H_
#define _HYPSI_DISCONTIC_H_

#include <complex>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

namespace HYPSI {

typedef std::complex<double> complex;

class DiscontIC {
public:

  double _dd_width; // width of the DD
  int _dd_dir; // direction of the DD normal, 0 for x, 1 for y, 2 for z
  xyzVector _Vshift_disc; // V field between the inferfaces
  xyzVector _Bvec_disc; // B field between the interfaces
  double _Vpar_disc; // V parallel in the region of the discontinuity
  double _Vperp_disc; // V perp in the region of the discontinuity
  int _force_free; // flag to tell if the discontinuity is force free or regular Harris
  double _Bguide; // Guide field in the force free case
  double _n_cs; // Current sheet density, as a fraction of the background n = (n_cs + 1)*n_0 at the centre

  UniformIC uniform_initial_conditions;

  DiscontIC( void ) {;}
  
  void read_input_data( ConfigData& config_data );

  void initialize_fields_and_particles( Hypsim& hypsim );
  
  void initialize_particle_sets_forcefree( Hypsim& hypsim );

  void initialize_particle_sets_harris( Hypsim& hypsim );

  void initialize_uniform_params( Hypsim& hypsim );

  void output_params( ostream& ostrm, string s="" ) const
  { 
    ostrm<<s<<"DiscontIC: Width = " << _dd_width  << " Direction: " << _dd_dir << "\n";
    ostrm<<s<<"DiscontIC: Bvec_disc = " << _Bvec_disc << " Vshift_disc = " << _Vshift_disc << "\n";
    ostrm<<s<<"DiscontIC: Vpar_disc = " << _Vpar_disc << " Vper_disc = " << _Vperp_disc << "\n";
  }


};


}   // end namespace HYPSI

#endif
