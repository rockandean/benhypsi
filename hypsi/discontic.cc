#include "hypsi.h"
#include "discontic.h"
#include "alfvenwic.h"

namespace HYPSI {

void DiscontIC::read_input_data( ConfigData& config_data )
{

// Start off by reading Uniform Plasma initial conditions

  try{

  uniform_initial_conditions.read_input_data( config_data );

  } catch( HypsiException& e )
  {
    e.push_err_msg(
   "In DiscontIC::read_input_data while reading UNIFORM PLASMA input data" );
    throw e;
  }

  //input parameters
  //double dd_width;
  //int dd_dir;
  vector<double> vxyz_disc;
  vector<double> Bxyz_disc;

  try{
    // this line for each input parameter
    config_data.get_data(
      "discont_initial_conditions/dd_width", _dd_width );   
    config_data.get_data(
      "discont_initial_conditions/dd_direction", _dd_dir );
    config_data.get_data(
      "discont_initial_conditions/Bxyz_disc", Bxyz_disc );
    config_data.get_data( 
      "discont_initial_conditions/Vxyz_disc", vxyz_disc );
    config_data.get_data(
      "discont_initial_conditions/Vpar_disc", _Vpar_disc );
    config_data.get_data(
      "discont_initial_conditions/Vperp_disc", _Vperp_disc );
    config_data.get_data(
      "discont_initial_conditions/force_free", _force_free );
   config_data.get_data(
      "discont_initial_conditions/Bguide", _Bguide );
   config_data.get_data(
      "discont_initial_conditions/n_cs", _n_cs );

  } catch(  KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get Discontinuity IC data from file: <"
         + config_data.filename() + ">",
      "DiscontIC::initialize_from_config_data" );
  }

  // any error handling on input variables

  if( (  _dd_dir < 0 || _dd_dir > 3 ) )
   throw HypsiException(
    "discontinuity_initial_conditions/dd_direction must be 0, 1 or 2 in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  if( Bxyz_disc.size() !=3 )
   throw HypsiException(
    "Insufficient data for discontinuity_initial_conditions/Bxyz_disc in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  _Bvec_disc._x = Bxyz_disc[0];
  _Bvec_disc._y = Bxyz_disc[1];
  _Bvec_disc._z = Bxyz_disc[2];

  if( vxyz_disc.size() !=3 )
   throw HypsiException(
    "Insufficient data for discontinuity_initial_conditions/Vxyz_disc in file: <"
       + config_data.filename() + ">",
   "DiscontIC::initialize_from_config_data" );

  _Vshift_disc._x = vxyz_disc[0];
  _Vshift_disc._y = vxyz_disc[1];
  _Vshift_disc._z = vxyz_disc[2];

} // end DiscontIC::read_input_data


void DiscontIC::initialize_uniform_params( Hypsim& hypsim )
{
// initialize UNIFORM PLASMA initial conditions

// uniform B field from calculation above, ie // to k vector

  hypsim.initialize_uniform_B( uniform_initial_conditions._Bvec );
  
  hypsim.initialize_uniform_resis( hypsim.sim_params.uniform_resis );
  hypsim.initialize_pe( hypsim.sim_params.initial_Te );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: About to initialize_particle_sets \n";

// cellrandom

  //uniform_initial_conditions.initialize_particle_sets( hypsim );
  if(_force_free == 1) {
    initialize_particle_sets_forcefree( hypsim );
  } else {
    initialize_particle_sets_harris( hypsim );
  }

// zonerandom
//  uniform_initial_conditions.initialize_particle_sets_zonerandom( hypsim );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: Done initialize_particle_sets \n";

}


void DiscontIC::initialize_fields_and_particles( Hypsim& hypsim )
{

  // Calculate the positions of the interfaces
  int c1, c2;
  double dd_frac, dd_rot, btot, theta;
  if (_dd_dir == 0) {
    c1 =  hypsim.domain_info.nxcell/4;
    c2 =  3*hypsim.domain_info.nxcell/4;
  }
  else if (_dd_dir == 1) {
    c1 =  hypsim.domain_info.nycell/4;
    c2 =  3*hypsim.domain_info.nycell/4;
  }
  else {
    c1 =  hypsim.domain_info.nzcell/4;
    c2 =  3*hypsim.domain_info.nzcell/4;
  }

  // Loop over all psets and particles
  for (int ipset=0; ipset<hypsim.sim_params.npsets; ipset++)
  for (int ip=0; ip<hypsim.psets[ipset]->np; ++ip)
  {
    double xp, yp, zp, vx, vy, vz;
    double pcell;
    xp = hypsim.psets[ipset]->pdata[ip*6]; 
    yp = hypsim.psets[ipset]->pdata[ip*6+1]; 
    zp = hypsim.psets[ipset]->pdata[ip*6+2];
    vx = hypsim.psets[ipset]->pdata[ip*6+3]; 
    vy = hypsim.psets[ipset]->pdata[ip*6+4]; 
    vz = hypsim.psets[ipset]->pdata[ip*6+5];
    
    // Find the cell coordinate of the particle in the direction of the discontinuity normal
    if (_dd_dir == 0) {
      pcell = xp/hypsim.sim_params.dxcell;
    }
    else if (_dd_dir == 1) {
      pcell = yp/hypsim.sim_params.dycell;
    }
    else {
      pcell = zp/hypsim.sim_params.dzcell;
    }

    // Calculate the fraction of V field taken from the discontinuity conditions
    dd_frac = 0.5*(tanh((pcell-c1)/_dd_width)-tanh((pcell-c2)/_dd_width));

    // Adjust the particle velocities based on this fraction, adjusted for the fact that
    // the background Vshift has already been added to the particle velocities in UniformIC
    vx += dd_frac*(_Vshift_disc._x - uniform_initial_conditions._pbimax_vxyzshift[0]);
    vy += dd_frac*(_Vshift_disc._y - uniform_initial_conditions._pbimax_vxyzshift[1]);
    vz += dd_frac*(_Vshift_disc._z - uniform_initial_conditions._pbimax_vxyzshift[2]);
    
    hypsim.psets[ipset]->pdata[ip*6+3] = vx;
    hypsim.psets[ipset]->pdata[ip*6+4] = vy;
    hypsim.psets[ipset]->pdata[ip*6+5] = vz;

  }

// CAN ADD IN THE DISCONTINUITY TO THE FIELDS HERE

// LOOP over all cells

  for( int ix = 1; ix <= hypsim.zinfo.nx; ++ix )
  for( int iy = 1; iy <= hypsim.zinfo.ny; ++iy )
  for( int iz = 1; iz <= hypsim.zinfo.nz; ++iz )
  {
    int ixyz;
    int cell;
    
    // Calculate the position of the cell based on the whole domain
    if (_dd_dir == 0) {
      cell = ix+hypsim.zinfo.ixcn*hypsim.zinfo.nx;
    }
    else if (_dd_dir == 1) {
      cell = iy+hypsim.zinfo.iycn*hypsim.zinfo.ny;
    }
    else {
      cell = iz+hypsim.zinfo.izcn*hypsim.zinfo.nz;
    }

    ixyz = hypsim.zfields->idx(ix,iy,iz);

    if (_force_free != 1) {
      // Calculate the fraction of B field taken from the discontinuity conditions
      dd_frac = 0.5*(tanh((cell-c1)/_dd_width)-tanh((cell-c2)/_dd_width));
     // modify the field values
      hypsim.zfields->Bx[ ixyz ] = _Bvec_disc._x*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._x;
      hypsim.zfields->By[ ixyz ] = _Bvec_disc._y*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._y;
      hypsim.zfields->Bz[ ixyz ] = _Bvec_disc._z*dd_frac + (1-dd_frac)*uniform_initial_conditions._Bvec._z;
    
    } else {
      dd_frac = (tanh((cell-c1)/_dd_width)-tanh((cell-c2)/_dd_width)-1);
      dd_rot = (1/cosh((cell-c1)/_dd_width)-1/cosh((cell-c2)/_dd_width));
      if (_dd_dir == 0 ) {
	btot = pow(pow(uniform_initial_conditions._Bvec._y,2)+pow(uniform_initial_conditions._Bvec._z,2),0.5);
	theta = atan(uniform_initial_conditions._Bvec._y/uniform_initial_conditions._Bvec._z);

	hypsim.zfields->Bx[ ixyz ] = uniform_initial_conditions._Bvec._x;
	hypsim.zfields->By[ ixyz ] = dd_frac*btot*cos(theta)+(dd_rot*btot+_Bguide)*sin(theta);
	hypsim.zfields->Bz[ ixyz ] = dd_frac*btot*sin(theta)-(dd_rot*btot+_Bguide)*cos(theta);
      } else if (_dd_dir==1){
	btot = pow(pow(uniform_initial_conditions._Bvec._x,2)+pow(uniform_initial_conditions._Bvec._z,2),0.5);
	theta = atan(uniform_initial_conditions._Bvec._x/uniform_initial_conditions._Bvec._z);

	hypsim.zfields->By[ ixyz ] = uniform_initial_conditions._Bvec._y;
	hypsim.zfields->Bx[ ixyz ] = dd_frac*btot*cos(theta)+(dd_rot*btot+_Bguide)*sin(theta);
	hypsim.zfields->Bz[ ixyz ] = dd_frac*btot*sin(theta)-(dd_rot*btot+_Bguide)*cos(theta);
      } else {
	btot = pow(pow(uniform_initial_conditions._Bvec._x,2)+pow(uniform_initial_conditions._Bvec._y,2),0.5);
	theta = atan(uniform_initial_conditions._Bvec._x/uniform_initial_conditions._Bvec._y);

	hypsim.zfields->Bz[ ixyz ] = uniform_initial_conditions._Bvec._z;
	hypsim.zfields->By[ ixyz ] = dd_frac*btot*cos(theta)+(dd_rot*btot+_Bguide)*sin(theta);
	hypsim.zfields->Bz[ ixyz ] = dd_frac*btot*sin(theta)-(dd_rot*btot+_Bguide)*cos(theta);


      }
    }
    
  }

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: About to make_B_xyzperiodic \n";

  MPI_Barrier( MPI_COMM_WORLD );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: after Barrier: About to make_B_xyzperiodic \n";

  hypsim.zfields->make_B_xyzperiodic();
  
//cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: Done make_B_xyzperiodic \n";

  hypsim.zhfields->copy_Bdata( *(hypsim.zfields) );



} // end DicontIC::initialize_fields_and_particles



void DiscontIC::initialize_particle_sets_forcefree( Hypsim& hypsim )
{

// set number of particle sets in simulation (set from value in UniformIC)

  hypsim.sim_params.npsets = uniform_initial_conditions._npsets;

// calculate total number particles in simulation

  uniform_initial_conditions._pninit_total_sim.resize( uniform_initial_conditions._npsets );

  for( int i=0; i<uniform_initial_conditions._npsets; ++i )
  {
    uniform_initial_conditions._pninit_total_sim[i] = uniform_initial_conditions._pninit_per_node[i] * hypsim.sim_params.n_zones;
  }

// resize the particle sets pointer array ...
  hypsim.psets.resize( uniform_initial_conditions._npsets );

// resize the particle zone moments pointer array for each particle set
  hypsim.pset_pzmoms.resize( uniform_initial_conditions._npsets );

  double ncell_domain =
      hypsim.sim_params.nxdomain * hypsim.sim_params.nydomain * hypsim.sim_params.nzdomain;

  double ncell_per_zone_f =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  int ncell_per_zone_i =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  for( int i=0; i<uniform_initial_conditions._npsets; ++i )
  {

// statistical weight for this particle set

    double statw;

// allocate the particle zone moments (for this node's zone)
//    - one for each particle set
// NB Zone must be initialized at this stage!

    hypsim.pset_pzmoms[i] = new PmoveZMoments( hypsim.zinfo );

// allocate ParticleSet object
    hypsim.psets[i] = new ParticleSet();

// statistical weight for this particle set
    statw = uniform_initial_conditions._pnden[i] / 
      ( uniform_initial_conditions._pninit_per_node[i] / ncell_per_zone_f );

    hypsim.psets[i]->initialize(
      uniform_initial_conditions._pnalloc_per_node[i],
      uniform_initial_conditions._pmass[i],
      uniform_initial_conditions._pcharge[i],
      statw,         // statistical weight
      uniform_initial_conditions._ptpflag[i],
      uniform_initial_conditions._psname [i] );


// velocity shift vector
    xyzVector pbimax_vxyzshift_vec;

    pbimax_vxyzshift_vec.set(
      uniform_initial_conditions._pbimax_vxyzshift[i*3], 
      uniform_initial_conditions._pbimax_vxyzshift[i*3+1], 
      uniform_initial_conditions._pbimax_vxyzshift[i*3+2] );

// argument required for initialize_uniform_bimax

    ZonePtclIdx pidx_range;

// intp_pninit_per_cell is number of ptcles per cell for every cell

// extra_np_inzone is number of "extra" particles
//    (to make total up to _pninit_per_node[i]) which are spread randomly
//    over zone. extra_np_inzone will be less than number of cells in zone

    int intp_pninit_per_cell, extra_np_inzone;

    intp_pninit_per_cell = 
      uniform_initial_conditions._pninit_per_node[i] / ncell_per_zone_i; // integer division
    extra_np_inzone = 
      uniform_initial_conditions._pninit_per_node[i] - intp_pninit_per_cell * ncell_per_zone_i;


    if( hypsim.zinfo.izone < 16 )
      cout<<"Z"<<hypsim.zinfo.izone<<": about to initialize_uniform_bimax ("
	  << intp_pninit_per_cell<<", "<<extra_np_inzone<<")\n";

    // First, calculate the positions of the interfaces
    double c1, c2;
    if (_dd_dir == 0) {
      c1 =  hypsim.domain_info.dxcell*hypsim.domain_info.nxcell/4;
      c2 =  hypsim.domain_info.dxcell*3*hypsim.domain_info.nxcell/4;
    }
    else if (_dd_dir == 1) {
      c1 =  hypsim.domain_info.dycell*hypsim.domain_info.nycell/4;
      c2 =  hypsim.domain_info.dycell*3*hypsim.domain_info.nycell/4;
    }
    else {
      c1 =  hypsim.domain_info.dzcell*hypsim.domain_info.nzcell/4;
      c2 =  hypsim.domain_info.dzcell*3*hypsim.domain_info.nzcell/4;
    }

    for( int ix = 0; ix < hypsim.zinfo.nx; ix++ )
    for( int iy = 0; iy < hypsim.zinfo.ny; iy++ )
    for( int iz = 0; iz < hypsim.zinfo.nz; iz++ )
    {

      xyzRegion cell_region;
      cell_region.set( hypsim.zinfo.region.x0 + ix*hypsim.zinfo.dx,
                       hypsim.zinfo.region.x0 + (ix+1)*hypsim.zinfo.dx,
                       hypsim.zinfo.region.y0 + iy*hypsim.zinfo.dy,
                       hypsim.zinfo.region.y0 + (iy+1)*hypsim.zinfo.dy,
                       hypsim.zinfo.region.z0 + iz*hypsim.zinfo.dz,
                       hypsim.zinfo.region.z0 + (iz+1)*hypsim.zinfo.dz );

      // Check here whether we're in the region of a the DD
      // If we are, use the discont values of the thermal velocities
      // Otherwise, just go on as normal

      bool in_dd_flag; // If this flag is 1, then
      in_dd_flag=false;
      if (_dd_dir == 0 && ( abs(cell_region.x0-c1) < _dd_width*hypsim.zinfo.dx )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 0 && ( abs(cell_region.x0-c2) < _dd_width*hypsim.zinfo.dx )) {
	in_dd_flag = true;
      }

      if (_dd_dir == 1 && ( abs(cell_region.y0-c1) < _dd_width*hypsim.zinfo.dy )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 1 && ( abs(cell_region.y0-c2) < _dd_width*hypsim.zinfo.dy )) {
	in_dd_flag = true;
      }

      if (_dd_dir == 2 && ( abs(cell_region.z0-c1) < _dd_width*hypsim.zinfo.dz )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 2 && ( abs(cell_region.z0-c2) < _dd_width*hypsim.zinfo.dz )) {
	in_dd_flag = true;
      }

      if (in_dd_flag==false) {

	//cout << "NotTD" << uniform_initial_conditions._pbimax_vthperp[i] << "\n";

      hypsim.psets[i]->initialize_uniform_bimax(

        intp_pninit_per_cell,
        &(hypsim.prng),
// only region of cell of this zone
        cell_region,
        uniform_initial_conditions._pbimax_vthpar[i],
        uniform_initial_conditions._pbimax_vthperp[i],
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );
      } 
      else {

	//cout << "In TD" << _Vpar_disc  << "\n";

      hypsim.psets[i]->initialize_uniform_bimax(

        intp_pninit_per_cell,
        &(hypsim.prng),
// only region of cell of this zone
        cell_region,
        _Vpar_disc,
        _Vperp_disc,
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );
      }

    }

    if( extra_np_inzone > 0 )

      hypsim.psets[i]->initialize_uniform_bimax(

        extra_np_inzone,

        &(hypsim.prng),

        hypsim.zinfo.region,   //  zone of this node

        uniform_initial_conditions._pbimax_vthpar[i],
        uniform_initial_conditions._pbimax_vthperp[i],
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );

  }  // end of loop over ipset


} // end DiscontIC::initialize_particle_sets_forcefree


void DiscontIC::initialize_particle_sets_harris( Hypsim& hypsim )
{

// set number of particle sets in simulation (set from value in UniformIC)

  hypsim.sim_params.npsets = uniform_initial_conditions._npsets;

// calculate total number particles in simulation

  uniform_initial_conditions._pninit_total_sim.resize( uniform_initial_conditions._npsets );

  for( int i=0; i<uniform_initial_conditions._npsets; ++i )
  {
    uniform_initial_conditions._pninit_total_sim[i] = uniform_initial_conditions._pninit_per_node[i] * hypsim.sim_params.n_zones;
  }

// resize the particle sets pointer array ...
  hypsim.psets.resize( uniform_initial_conditions._npsets );

// resize the particle zone moments pointer array for each particle set
  hypsim.pset_pzmoms.resize( uniform_initial_conditions._npsets );

  double ncell_domain =
      hypsim.sim_params.nxdomain * hypsim.sim_params.nydomain * hypsim.sim_params.nzdomain;

  double ncell_per_zone_f =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  int ncell_per_zone_i =
      hypsim.zinfo.nx * hypsim.zinfo.ny * hypsim.zinfo.nz;

  for( int i=0; i<uniform_initial_conditions._npsets; ++i )
  {

// statistical weight for this particle set

    double statw;

// allocate the particle zone moments (for this node's zone)
//    - one for each particle set
// NB Zone must be initialized at this stage!

    hypsim.pset_pzmoms[i] = new PmoveZMoments( hypsim.zinfo );

// allocate ParticleSet object
    hypsim.psets[i] = new ParticleSet();

// statistical weight for this particle set
    statw = uniform_initial_conditions._pnden[i] / 
      ( uniform_initial_conditions._pninit_per_node[i] / ncell_per_zone_f );

    hypsim.psets[i]->initialize(
      uniform_initial_conditions._pnalloc_per_node[i],
      uniform_initial_conditions._pmass[i],
      uniform_initial_conditions._pcharge[i],
      statw,         // statistical weight
      uniform_initial_conditions._ptpflag[i],
      uniform_initial_conditions._psname [i] );


// velocity shift vector
    xyzVector pbimax_vxyzshift_vec;

    pbimax_vxyzshift_vec.set(
      uniform_initial_conditions._pbimax_vxyzshift[i*3], 
      uniform_initial_conditions._pbimax_vxyzshift[i*3+1], 
      uniform_initial_conditions._pbimax_vxyzshift[i*3+2] );

// argument required for initialize_uniform_bimax

    ZonePtclIdx pidx_range;

// intp_pninit_per_cell is number of ptcles per cell for every cell

// extra_np_inzone is number of "extra" particles
//    (to make total up to _pninit_per_node[i]) which are spread randomly
//    over zone. extra_np_inzone will be less than number of cells in zone

    int intp_pninit_per_cell, extra_np_inzone;

    intp_pninit_per_cell = 
      uniform_initial_conditions._pninit_per_node[i] / ncell_per_zone_i; // integer division
    extra_np_inzone = 
      uniform_initial_conditions._pninit_per_node[i] - intp_pninit_per_cell * ncell_per_zone_i;


    if( hypsim.zinfo.izone < 16 )
      cout<<"Z"<<hypsim.zinfo.izone<<": about to initialize_uniform_bimax ("
	  << intp_pninit_per_cell<<", "<<extra_np_inzone<<")\n";

    double c1, c2;
    int cell;
    int intp_pnharris_this_cell;
    bool in_dd_flag;
    double Bmag;
    Bmag = sqrt(pow(uniform_initial_conditions._Bvec._x,2)
		+pow(uniform_initial_conditions._Bvec._y,2)
		+pow(uniform_initial_conditions._Bvec._z,2));



    // First, calculate the positions of the interfaces
    if (_dd_dir == 0) {
      c1 =  hypsim.domain_info.nxcell/4;
      c2 =  3*hypsim.domain_info.nxcell/4;
    }
    else if (_dd_dir == 1) {
      c1 =  hypsim.domain_info.nycell/4;
      c2 =  3*hypsim.domain_info.nycell/4;
    }
    else {
      c1 =  hypsim.domain_info.nzcell/4;
      c2 =  3*hypsim.domain_info.nzcell/4;
    }

    for( int ix = 0; ix < hypsim.zinfo.nx; ix++ )
    for( int iy = 0; iy < hypsim.zinfo.ny; iy++ )
    for( int iz = 0; iz < hypsim.zinfo.nz; iz++ )
    {

      xyzRegion cell_region;
      cell_region.set( hypsim.zinfo.region.x0 + ix*hypsim.zinfo.dx,
                       hypsim.zinfo.region.x0 + (ix+1)*hypsim.zinfo.dx,
                       hypsim.zinfo.region.y0 + iy*hypsim.zinfo.dy,
                       hypsim.zinfo.region.y0 + (iy+1)*hypsim.zinfo.dy,
                       hypsim.zinfo.region.z0 + iz*hypsim.zinfo.dz,
                       hypsim.zinfo.region.z0 + (iz+1)*hypsim.zinfo.dz );

      // Calculate the position of the cell based on the whole domain
      
      if (_dd_dir == 0) {
	cell = ix+hypsim.zinfo.ixcn*hypsim.zinfo.nx;
	//_n_cs = pow(_Vperp_disc,-2)*
	//  (1-pow(uniform_initial_conditions._Bvec._x/uniform_initial_conditions._Bvec.mag,2))
	//  +pow(_Vperp_disc/uniform_initial_conditions._pbimax_vthperp[i],2);
	//_n_cs = pow(_Vperp_disc,-2)*
	//  (1-pow(uniform_initial_conditions._Bvec._x/Bmag,2))
	//  +pow(_Vperp_disc/uniform_initial_conditions._pbimax_vthperp[i],2);
      }
      else if (_dd_dir == 1) {
	cell = iy+hypsim.zinfo.iycn*hypsim.zinfo.ny;
	//_n_cs = pow(_Vperp_disc,-2)*
	//  (1-pow(uniform_initial_conditions._Bvec._y/Bmag,2))
	//  +pow(_Vperp_disc/uniform_initial_conditions._pbimax_vthperp[i],2);
      }
      else {
	cell = iz+hypsim.zinfo.izcn*hypsim.zinfo.nz;
	//_n_cs = pow(_Vperp_disc,-2)*
	//  (1-pow(uniform_initial_conditions._Bvec._z/Bmag,2))
	//  +pow(_Vperp_disc/uniform_initial_conditions._pbimax_vthperp[i],2);
      }

      // Calculate the number of particles in this cell

      // _n_cs = pow(_Vperp_disc,-2)*(1-pow(,2))
      //_n_cs = 5;

      intp_pnharris_this_cell = intp_pninit_per_cell*
	(1+(_n_cs-1)/pow(cosh((cell-c1)/_dd_width),2)+(_n_cs-1)/pow(cosh((cell-c2)/_dd_width),2));

      // Check here whether we're in the region of a the DD
      // If we are, use the discont values of the thermal velocities
      // Otherwise, just go on as normal

      in_dd_flag=false;
      if (_dd_dir == 0 && ( abs(cell_region.x0-hypsim.domain_info.dxcell*c1) < _dd_width*hypsim.zinfo.dx )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 0 && ( abs(cell_region.x0-hypsim.domain_info.dxcell*c2) < _dd_width*hypsim.zinfo.dx )) {
	in_dd_flag = true;
      }

      if (_dd_dir == 1 && ( abs(cell_region.y0-hypsim.domain_info.dycell*c1) < _dd_width*hypsim.zinfo.dy )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 1 && ( abs(cell_region.y0-hypsim.domain_info.dycell*c2) < _dd_width*hypsim.zinfo.dy )) {
	in_dd_flag = true;
      }

      if (_dd_dir == 2 && ( abs(cell_region.z0-hypsim.domain_info.dzcell*c1) < _dd_width*hypsim.zinfo.dz )) {
	in_dd_flag = true;
      }
      if (_dd_dir == 2 && ( abs(cell_region.z0-hypsim.domain_info.dzcell*c2) < _dd_width*hypsim.zinfo.dz )) {
	in_dd_flag = true;
      }
      
      if (in_dd_flag==false) {

      hypsim.psets[i]->initialize_uniform_bimax(

        intp_pnharris_this_cell,
        &(hypsim.prng),
// only region of cell of this zone
        cell_region,
        uniform_initial_conditions._pbimax_vthpar[i],
        uniform_initial_conditions._pbimax_vthperp[i],
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );
      } 
      else {

      hypsim.psets[i]->initialize_uniform_bimax(

        intp_pnharris_this_cell,
        &(hypsim.prng),
// only region of cell of this zone
        cell_region,
        _Vpar_disc,
        _Vperp_disc,
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );
      }

    }

    if( extra_np_inzone > 0 )

      hypsim.psets[i]->initialize_uniform_bimax(

        extra_np_inzone,

        &(hypsim.prng),

        hypsim.zinfo.region,   //  zone of this node

        uniform_initial_conditions._pbimax_vthpar[i],
        uniform_initial_conditions._pbimax_vthperp[i],
        uniform_initial_conditions._Bvec,
        uniform_initial_conditions._pbimax_vparshift[i],
        pbimax_vxyzshift_vec,
        pidx_range );

  }  // end of loop over ipset

} // end DiscontIC::initialize_particle_sets_harris


} // end namespace HYPSI
