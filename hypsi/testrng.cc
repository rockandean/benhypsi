#include <iostream>

#include "rng.h"

using namespace HYPSI;
using namespace std;

main()
{

  int iseed;
  
  cout <<"Input seed: "; cin>>iseed;
  
  RNG rng( iseed );
  
  for(int i=0;i<10;++i)
  {
  
  cout << i <<":  "<< rng() << "\n";
  cout << i <<":  "<< rng()*10.0 << "\n";

  }



}
