//
// output.h
//
// Framework classes for hypsi output
//
// D. Burgess

#ifndef _HYPSI_OUTPUT_H_
#define _HYPSI_OUTPUT_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <list>

#include "exception.h"

namespace HYPSI {

using namespace std;

class OutputException : public HypsiException {
public:
  OutputException( const std::string& err_str, const std::string fn_str="",
                   int sys_errno = 0 )
    : HypsiException( err_str, fn_str, sys_errno )
   { _type_str = "OutputException"; }
  OutputException( const char* err_str, const char* fn_str="",
                   int sys_errno = 0 )
    : HypsiException( err_str, fn_str, sys_errno )
   { _type_str = "OutputException"; }



};

/*! \brief Class for dimensionality of rectangular arrays

*/
class Dimens {

  std::vector<int> _dimens;

  friend class OutputAdaptor;

public:
  Dimens( void ) {;}
  Dimens( const Dimens& dimens )
    : _dimens( dimens._dimens ) {;}
  Dimens( const std::vector<int>& dimens )
    : _dimens( dimens ) {;}
  Dimens( int d1 ) : _dimens(1) { _dimens[0]=d1; }
  Dimens( int d1, int d2 )
   : _dimens(2) { _dimens[0]=d1; _dimens[1]=d2;}
  Dimens( int d1, int d2, int d3 )
   : _dimens(3) { _dimens[0]=d1; _dimens[1]=d2; _dimens[2]=d3; }
  Dimens( int d1, int d2, int d3, int d4 )
   : _dimens(4) { _dimens[0]=d1; _dimens[1]=d2;
                  _dimens[2]=d3; _dimens[3]=d4; }

  int size( void ) const { return _dimens.size(); }
  int operator[]( const int i ) const { return _dimens[i]; }
  int nels( void ) const
  { int n=1; for(int i=0; i<_dimens.size(); ++i ) n *= _dimens[i];
    return n; }

};

/* \brief Virtual base class for output adaptor

*/
class OutputAdaptor {

public:
  OutputAdaptor( void ) {;}

  virtual void open( const std::string& outf )
  { throw OutputException("Function not implemented","HYPSI::OutputAdaptor::open");}

  virtual void close( void )
  { throw OutputException("Function not implemented","HYPSI::OutputAdaptor::close");}

  virtual void flush( void )
  { throw OutputException("Function not implemented","HYPSI::OutputAdaptor::flush");}

// write int functions
  virtual void write( const std::string& objname, int i )
  { throw OutputException("Function not implemented","HYPSI::OutputAdaptor::write(int)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const int* i_array )
  { throw OutputException("Function not implemented",
         "HYPSI::OutputAdaptor::write(int* array)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const long* i_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(long* array)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const std::vector<int>& i_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(vector<int> array)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const std::vector<long>& i_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(vector<long> array)");}

// write float functions
  virtual void write( const std::string& objname, float f )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(float)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const float* f_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(float* array)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const std::vector<float>& f_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(vector<float> array)");}

// write double functions
  virtual void write( const std::string& objname, double d )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(double)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const double* d_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(double* array)");}

  virtual void write( const std::string& objname,
                      const Dimens dimens,
                      const std::vector<double>& d_array )
  { throw OutputException("Function not implemented",
      "HYPSI::OutputAdaptor::write(vector<double> array)");}

// write string functions
  virtual void write( const std::string& objname, const std::string& s )
  { throw OutputException("Function not implemented","HYPSI::OutputAdaptor::write(int)");}


};

class OutputAgentBase {

public:
  OutputAgentBase( void ) {;}
  OutputAgentBase( const OutputAgentBase& a ) {;}

  virtual void output( const std::string& tag, int cycle ) = 0;
//  virtual void output( const std::string& tag, int cycle, int sample ) = 0;
  
  virtual void open( const std::string& outf ) = 0;
  virtual void close( void ) = 0;
  
  
};

/* \brief Base class for OutputAgents using template for output adaptor


*/

template <class Toa> class OutputAgent : public OutputAgentBase {

protected:
Toa output_adaptor;

public:
  OutputAgent( void ) {;}
  OutputAgent( const OutputAgent& a ) {;}

  virtual void output( const std::string& tag, int cycle) = 0;
//  virtual void output( const std::string& tag, int cycle, int sample) = 0;
  
  void open( const std::string& outf ) { output_adaptor.open( outf ); }
  void close( void ) { output_adaptor.close(); }
  void flush( void ) { output_adaptor.flush(); }

};

/* \brief Container (list) class for OutputAgents


*/

//template <class Toa>

class OutputManager {

  std::list< OutputAgentBase* > agents_list;

public:
  OutputManager( void ) {;}
  
  void push_back( OutputAgentBase * a_p ) 
  {  agents_list.push_back( a_p ); }

  void output( const std::string& tag, int cycle )
  {  
//    typename
    std::list< OutputAgentBase* >::iterator p = agents_list.begin();
    while( p != agents_list.end() )
      (*p++)->output( tag, cycle );
  }

/*
  void output( const std::string& tag, int cycle, int sample)
  {  
    typename std::list< OutputAgentBase* >::iterator p = agents_list.begin();
    while( p != agents_list.end() )
      (*p++)->output( tag, cycle, sample );
  }
*/

};


// ======================================================================


class coutOutputAdaptor : public OutputAdaptor {

public:
  coutOutputAdaptor( void ) {;}


  void open( const std::string& outf )
  {  std::cout << "coutOutputAdaptor open() file: " << outf << "\n" ; }
  void close( void )
  {  std::cout << "coutOutputAdaptor close()\n" ; }
  void flush( void )
  {  std::cout << "coutOutputAdaptor flush()" << endl; }
  

// write int functions
  void write( const std::string& objname, int i )
  {  std::cout << "coutOutputAdaptor write int: <"<<objname<< "> : " << i << "\n"; }

  void write( const std::string& objname,
              const Dimens dimens, const int* i_array )
  {  std::cout << "coutOutputAdaptor write int* array: <"<<objname<< "> : " << "\n"; }
  void write( const std::string& objname,
              const Dimens dimens, const std::vector<int>& i_array )
  {  std::cout << "coutOutputAdaptor write vector<int> array: <"<<objname<< "> : " << "\n"; }


// write float functions
  void write( const std::string& objname, float f )
  {  std::cout << "coutOutputAdaptor write float: <"<<objname<< "> : " << f << "\n"; }
  void write( const std::string& objname,
                        const Dimens dimens,
                        const float* f_array )
  {  std::cout << "coutOutputAdaptor write float* array: <"<<objname<< "> : " << "\n"; }
  void write( const std::string& objname,
                        const Dimens dimens,
                        const std::vector<float>& f_array )
  {  std::cout << "coutOutputAdaptor write vector<float> array: <"<<objname<< "> : " << "\n"; }

// write double functions
  void write( const std::string& objname, double d )
  {  std::cout << "coutOutputAdaptor write double: <"<<objname<< "> : " << d << "\n"; }
  void write( const std::string& objname,
                        const Dimens dimens,
                        const double* d_array )
  {  std::cout << "coutOutputAdaptor write double* array: <"<<objname<< "> : " << "\n"; }
  void write( const std::string& objname,
                        const Dimens dimens,
                        const std::vector<double>& d_array )
  {  std::cout << "coutOutputAdaptor write vector<double> array: <"<<objname<< "> : " << "\n"; }

// write strings
  void write( const std::string& objname, string s )
  {  std::cout << "coutOutputAdaptor write string: <"<<objname<< "> : " << s << "\n"; }

};


template <class Toa> class HypsiOutputAgent : public OutputAgent< Toa > 
{

  Hypsim* _hypsim;

public:
  HypsiOutputAgent( Hypsim* hypsim ) : _hypsim(hypsim) {;}
  
private:

  string int2string( int i )
  {
    stringstream strs; strs.str( "" ); strs << i;
    return strs.str();
  }


public:
	
/** method to write to disk. Acceptable tags are:


*/
		
void output( const string& tag, int icycle )
{
  string zone_str;
  string cycle_str;
  
  stringstream strs;
  
  strs << _hypsim->sim_params.hostzone;
  
  zone_str = strs.str();
  
  strs.str( "" ); strs << icycle; cycle_str = strs.str();
  
  if( tag.find("ParticleData",0) != string::npos )
  {
    for( int isample=0;
         isample<_hypsim->pdata_output_control._nsamples; ++isample )
    {
    string tagroot, sample_str; stringstream sstr;
    sstr.str(""); sstr<<isample; sample_str = sstr.str();
    tagroot = "/ParticleData/timestep_"+cycle_str
                +"/sample_"+sample_str+"/";

    vector<double> region_vec;
    _hypsim->pdata_output_control._region_list[isample].get_vector( region_vec );
    this->output_adaptor.write( tagroot+"region", Dimens(6), region_vec );
    this->output_adaptor.write(
      tagroot+"pset_list",
      Dimens(_hypsim->pdata_output_control._psets_lists[isample].size()),
      _hypsim->pdata_output_control._psets_lists[isample] );
    
    for( int ipset=0;
         ipset< _hypsim->pdata_output_control._psets_lists[isample].size();
         ++ipset )
    {
    sstr.str(""); sstr<<ipset;
    tagroot = "/ParticleData/timestep_"+cycle_str
                +"/sample_"+sample_str+"/pset_"+sstr.str()+"/";
    double* pdata = _hypsim->psets[ipset]->pdata;
    int np = _hypsim->psets[ipset]->np;
    xyzRegion& region = _hypsim->pdata_output_control._region_list[isample];
    int np_in_sample = 0;
    for( int ip=0; ip<np;
             ip+= _hypsim->pdata_output_control._nptcle_cadence[isample])
    {
      if( region.in_region( pdata[ip*6], pdata[ip*6+1], pdata[ip*6+2] ) )
        np_in_sample++;
    }// end loop over particles

    this->output_adaptor.write( tagroot+"npdata",np_in_sample );
    
    if( np_in_sample != 0 )
    {
    float* fpdata = new float[ np_in_sample*6 ];
    float* fpdata_p = fpdata;
    for( int ip=0; ip<np;
             ip+= _hypsim->pdata_output_control._nptcle_cadence[isample])
    {
      if( region.in_region( pdata[ip*6], pdata[ip*6+1], pdata[ip*6+2] ) )
        for( int i=0; i<6; ++i )
          *fpdata_p++ = static_cast<float>(pdata[ ip*6 + i ]);
    }// end loop over particles

    this->output_adaptor.write(
      tagroot+"pdata", Dimens(np_in_sample,6), fpdata );

    delete[] fpdata;
    }

    } // end of loop over particle sets
    } // end of loop over samples
  
  }  // end of if ParticleData

  if( tag.find("SimulationParameters",0) != string::npos )
  {

    this->output_adaptor.write(
      "/SimulationParameters/hostzone",_hypsim->sim_params.hostzone );
    this->output_adaptor.write(
      "/SimulationParameters/nzones",_hypsim->sim_params.n_zones );
    this->output_adaptor.write(
      "/SimulationParameters/npsets",_hypsim->sim_params.npsets );
    this->output_adaptor.write(
      "/SimulationParameters/nsubsteps",_hypsim->sim_params.nsubsteps );
    this->output_adaptor.write(
      "/SimulationParameters/prng_seed",_hypsim->sim_params.prng_seed );
    this->output_adaptor.write(
      "/SimulationParameters/uniform_resis",_hypsim->sim_params.uniform_resis );
    this->output_adaptor.write(
      "/SimulationParameters/initial_Te",_hypsim->sim_params.initial_Te );
    this->output_adaptor.write(
      "/SimulationParameters/electron_gamma",_hypsim->sim_params.electron_gamma );
    this->output_adaptor.write(
      "/SimulationParameters/dt",_hypsim->sim_params.dt );
    this->output_adaptor.write(
      "/SimulationParameters/nt",_hypsim->sim_params.ntimesteps);
    
// \todo problem with string is that there is no overload for a vector <string> in HDF output adaptor
    this->output_adaptor.write(
     "/SimulationParameters/psmass", Dimens(_hypsim->sim_params.npsets) ,_hypsim->sim_params.psmass);
    this->output_adaptor.write(
     "/SimulationParameters/pscharge", Dimens(_hypsim->sim_params.npsets), _hypsim->sim_params.pscharge);
    // fix passing all names separated by comas.
    string psname_str;
    stringstream sstr("");
    for( int ipset=0; ipset < _hypsim->sim_params.npsets; ++ipset )
    {
    sstr<<_hypsim->sim_params.psname[ipset];
    if (ipset+1<_hypsim->sim_params.npsets)sstr<<",";
    }
    psname_str = sstr.str();
    this->output_adaptor.write(
     "/SimulationParameters/psname", psname_str.c_str());
    this->output_adaptor.write(
     "/SimulationParameters/pnden", Dimens(_hypsim->sim_params.npsets) , _hypsim->sim_params.pnden );
   
    vector<double> vd(3); vd[0]=_hypsim->sim_params.dxcell;
    vd[1]=_hypsim->sim_params.dycell; vd[2]=_hypsim->sim_params.dzcell;
    this->output_adaptor.write(
      "/SimulationParameters/dxdydzcell", Dimens(3), vd );

    vector<int> vi(3); vi[0]=_hypsim->sim_params.nxdomain;
    vi[1]=_hypsim->sim_params.nydomain; vi[2]=_hypsim->sim_params.nzdomain;
    this->output_adaptor.write(
      "/SimulationParameters/nxnynzdomain", Dimens(3), vi );

    vector<double> regionvec;
    _hypsim->sim_params.domain_region.get_vector( regionvec );
    this->output_adaptor.write(
      "/SimulationParameters/zinfo/xyz_domain", Dimens(6), regionvec );

    vd[0] = _hypsim->sim_params.initial_Bvec[0];
    vd[1] = _hypsim->sim_params.initial_Bvec[1];
    vd[2] = _hypsim->sim_params.initial_Bvec[2];
    this->output_adaptor.write(
      "/SimulationParameters/initial_Bvec", Dimens(3), vd );
      
    int myaux2=(int)_hypsim->ts_output_control._control_list.size();
    vector<int> myaux(myaux2);

    for (int i=0 ; i < myaux2; ++i ){
        myaux[i]=_hypsim->ts_output_control._control_list[i]._tscadence;
    }
    this->output_adaptor.write(
          "/SimulationParameters/fieldCadence", Dimens(myaux2), myaux);

/*/! \todo check this first, and then find a way to save all tags.
    this->output_adaptor.write(
          "/SimulationParameters/fieldCadence", _hypsim->ts_output_control._control_list[0]._tscadence);
*/
    this->output_adaptor.write(
      "/SimulationParameters/zinfo/zone", _hypsim->zinfo.izone );
    
    vi[0]=_hypsim->zinfo.nx;
    vi[1]=_hypsim->zinfo.ny; vi[2]=_hypsim->zinfo.nz;
    this->output_adaptor.write(
      "/SimulationParameters/zinfo/nxnynz", Dimens(3), vi );

    vd[0] = _hypsim->zinfo.dx;
    vd[1] = _hypsim->zinfo.dy;
    vd[2] = _hypsim->zinfo.dz;
    this->output_adaptor.write(
      "/SimulationParameters/zinfo/dxdydz", Dimens(3), vd );

    _hypsim->zinfo.region.get_vector( regionvec );
    this->output_adaptor.write(
      "/SimulationParameters/zinfo/region", Dimens(6), regionvec );

    
  } // end of if "SimulationParameters"

  if( tag.find("Bfield",0) != string::npos )
  {
    string objroot = "TimeStep/"+cycle_str;
    this->output_adaptor.write(
      objroot+"/ZoneFields/Bx",
      Dimens(_hypsim->zfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->Bx );
    this->output_adaptor.write(
      objroot+"/ZoneFields/By",
      Dimens(_hypsim->zfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->By );
    this->output_adaptor.write(
      objroot+"/ZoneFields/Bz",
      Dimens(_hypsim->zfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->Bz );
  } // end of if "Bfield"

  if( tag.find("Efield",0) != string::npos )
  {
    string objroot = "TimeStep/"+cycle_str;
    this->output_adaptor.write(
      objroot+"/ZoneFields/Ex",
      Dimens(_hypsim->zfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->Ex );
    this->output_adaptor.write(
      objroot+"/ZoneFields/Ey",
      Dimens(_hypsim->zfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->Ey );
    this->output_adaptor.write(
      objroot+"/ZoneFields/Ez",
      Dimens(_hypsim->zhfields->nx2,_hypsim->zfields->ny2,_hypsim->zfields->nz2),
      _hypsim->zfields->Ez );
  } // end of if "Efield"


  if( tag.find("NVmoments",0) != string::npos )
  {
    string objroot;
    for( int ipset=0; ipset < _hypsim->sim_params.npsets; ++ipset )
    {
    objroot = "TimeStep/"+cycle_str+"/Pset/"+int2string(ipset);
    
/*    cout << "Output: " << objroot << "Dims: " 
         << _hypsim->pset_nvtmoms[ipset]->nx2 << ", "
         << _hypsim->pset_nvtmoms[ipset]->ny2 << ", "
         << _hypsim->pset_nvtmoms[ipset]->nz2 << "\n";
*/
    
    this->output_adaptor.write(
      objroot+"/ZoneMoments/dn",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->dn );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/dnB",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->dnB );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vx",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Vx );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vy",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Vy );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vz",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Vz );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/VxB",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->VxB );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/VyB",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->VyB );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/VzB",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->VzB );

    } // end of for loop over particle sets
  } // end of if "NVmoments"

if( tag.find("Tmoments",0) != string::npos )
  {
    string objroot;
    for( int ipset=0; ipset < _hypsim->sim_params.npsets; ++ipset )
    {
    objroot = "TimeStep/"+cycle_str+"/Pset/"+int2string(ipset);
    
    /*
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Txx",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Txx );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tyy",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Tyy );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tzz",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Tzz );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Txy",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Txy );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Txz",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Txz );
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tyz",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      _hypsim->pset_nvtmoms[ipset]->Tyz );
    /*

    /*
    double *G;
    double *Tpar, *Tperp1, *Tperp2;
    ScalarZarray Tpar_sza, Tperp1_sza, Tperp2_sza;
    int nx;
    int ny;
    int nz;
    int nxyz, nyz2, nxyz2;
    
    nx = _hypsim->zinfo.nx;
    ny = _hypsim->zinfo.ny;
    nz = _hypsim->zinfo.nz;
    nxyz2 = (nz+2) * ((ny+2)*(nz+2)+2);

    G = new double[ 3*nxyz2 ];
    Tpar =   G;
    Tperp1 = G +   nxyz2;
    Tperp2 = G + 2*nxyz2;
    Tpar_sza.initialize( _hypsim->zinfo , Tpar );
    Tperp1_sza.initialize( _hypsim->zinfo , Tperp1 );
    Tperp2_sza.initialize( _hypsim->zinfo , Tperp2 );

    // Note: these calculations assume the direction of the B field is constant within a grid cell. 
    // A more accurate calculation would need to do the rotation in the moment calculation
    for( int im=0; im < nxyz2; ++im )
	  {
	    Tpar[im] = (_hypsim->pset_nvtmoms[ipset]->Txx[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] +
			_hypsim->pset_nvtmoms[ipset]->Tyy[im] * _hypsim->zfields->By[im] * _hypsim->zfields->By[im] +
			_hypsim->pset_nvtmoms[ipset]->Tzz[im] * _hypsim->zfields->Bz[im] * _hypsim->zfields->Bz[im] +
			2*_hypsim->pset_nvtmoms[ipset]->Txy[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->By[im] +
			2*_hypsim->pset_nvtmoms[ipset]->Txz[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bz[im] +
			2*_hypsim->pset_nvtmoms[ipset]->Tyz[im] * _hypsim->zfields->By[im] * _hypsim->zfields->Bz[im] )
	      / ( _hypsim->zfields->Bx[im]*_hypsim->zfields->Bx[im] +
		  _hypsim->zfields->By[im]*_hypsim->zfields->By[im] +
		  _hypsim->zfields->Bz[im]*_hypsim->zfields->Bz[im] );

	    Tperp1[im] = ( _hypsim->pset_nvtmoms[ipset]->Txx[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bz[im] * _hypsim->zfields->Bz[im] 
			   / (_hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] + _hypsim->zfields->By[im] * _hypsim->zfields->By[im]) +
			   _hypsim->pset_nvtmoms[ipset]->Tyy[im] * _hypsim->zfields->By[im] * _hypsim->zfields->By[im] * _hypsim->zfields->Bz[im] * _hypsim->zfields->Bz[im] 
			   / (_hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] + _hypsim->zfields->By[im] * _hypsim->zfields->By[im]) +
			   _hypsim->pset_nvtmoms[ipset]->Tzz[im] * (_hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] + _hypsim->zfields->By[im] * _hypsim->zfields->By[im]) +
			   2*_hypsim->pset_nvtmoms[ipset]->Txy[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->By[im] * _hypsim->zfields->Bz[im] * _hypsim->zfields->Bz[im] 
			   / (_hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] + _hypsim->zfields->By[im] * _hypsim->zfields->By[im]) -
			   2*_hypsim->pset_nvtmoms[ipset]->Txz[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bz[im] -
			   2*_hypsim->pset_nvtmoms[ipset]->Tyz[im] * _hypsim->zfields->By[im] * _hypsim->zfields->Bz[im] )
	      / ( _hypsim->zfields->Bx[im]*_hypsim->zfields->Bx[im] +
		  _hypsim->zfields->By[im]*_hypsim->zfields->By[im] +
		  _hypsim->zfields->Bz[im]*_hypsim->zfields->Bz[im] );

	    Tperp2[im] = ( _hypsim->pset_nvtmoms[ipset]->Txx[im] * _hypsim->zfields->By[im] * _hypsim->zfields->By[im] +
			   _hypsim->pset_nvtmoms[ipset]->Tyy[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->Bx[im] -
			   2*_hypsim->pset_nvtmoms[ipset]->Txy[im] * _hypsim->zfields->Bx[im] * _hypsim->zfields->By[im] )
	      / ( _hypsim->zfields->Bx[im]*_hypsim->zfields->Bx[im] +
		  _hypsim->zfields->By[im]*_hypsim->zfields->By[im] );

	  }
    
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tpar",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      Tpar );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tperp1",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      Tperp1 );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tperp2",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
      Tperp2 );

    */
    // These from the actual moments
    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vpar",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Vpar );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vperp1",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Vperp1 );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Vperp2",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Vperp2 );

   this->output_adaptor.write(
      objroot+"/ZoneMoments/Tpar",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Tpar );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tperp1",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Tperp1 );

    this->output_adaptor.write(
      objroot+"/ZoneMoments/Tperp2",
      Dimens(_hypsim->pset_nvtmoms[ipset]->nx2,
             _hypsim->pset_nvtmoms[ipset]->ny2,
             _hypsim->pset_nvtmoms[ipset]->nz2),
       _hypsim->pset_nvtmoms[ipset]->Tperp2 );

    } // end of for loop over particle sets
  } // end of if "Tmoments"


  if( tag.find("ParticleStats",0) != string::npos )
  {
    stringstream strs;

    for( int ipset=0; ipset < _hypsim->sim_params.npsets; ++ipset )
    {

      _hypsim->psets[ipset]->calc_stats();

      strs.str("");
      strs << ipset;
      string objroot = "TimeStep/" + cycle_str
                       + "/ParticleStats/PSet_" + strs.str();

      this->output_adaptor.write( objroot + "/np",
              _hypsim->psets[ipset]->get_np() );
      this->output_adaptor.write( objroot + "/nalloc",
              _hypsim->psets[ipset]->get_nalloc() );

      this->output_adaptor.write( objroot + "/pmass",
	      _hypsim->psets[ipset]->get_m() );
      this->output_adaptor.write( objroot + "/pcharge",
	      _hypsim->psets[ipset]->get_q() );

      this->output_adaptor.write( objroot + "/pname",
           _hypsim->psets[ipset]->type() );


 
      vector<double> vdminmax, vdmeans;
      double vsq_sum;
      _hypsim->psets[ipset]->get_stats_output_data(vdminmax,vdmeans,vsq_sum);

      this->output_adaptor.write( objroot + "/vxvyvzv_minmax",
              Dimens(8), vdminmax );
      this->output_adaptor.write( objroot + "/vxvyvzv_mean",
              Dimens(4), vdmeans );
      this->output_adaptor.write( objroot + "/vsq_sum", vsq_sum );

    }
  } // end of if "ParticleStats"

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  if( tag.find("ZoneFieldStats",0) != string::npos )
  {

    _hypsim->zfields->calc_stats();

    string objroot = "TimeStep/" + cycle_str + "/ZoneFieldStats";
    
    Dimens dimen8(8);
    Dimens dimen4(4);

    this->output_adaptor.write( objroot + "/Bxyzt_minmax",
            dimen8, _hypsim->zfields->stats.Bxyzt_minmax );
    this->output_adaptor.write( objroot + "/Exyzt_minmax",
            dimen8, _hypsim->zfields->stats.Exyzt_minmax );
    this->output_adaptor.write( objroot + "/Bxyzt_mean",
            dimen4, _hypsim->zfields->stats.Bxyzt_mean );
    this->output_adaptor.write( objroot + "/Exyzt_mean",
            dimen4, _hypsim->zfields->stats.Exyzt_mean );
    this->output_adaptor.write( objroot + "/Bxyzt_sqsum",
            dimen4, _hypsim->zfields->stats.Bxyzt_sqsum );
    this->output_adaptor.write( objroot + "/Exyzt_sqsum",
            dimen4, _hypsim->zfields->stats.Exyzt_sqsum );
    this->output_adaptor.write( objroot + "/Bxyzt_variance",
            dimen4, _hypsim->zfields->stats.Bxyzt_variance );
    this->output_adaptor.write( objroot + "/Exyzt_variance",
            dimen4, _hypsim->zfields->stats.Exyzt_variance );

  } // end of if "ZoneFieldStats"

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

} // end of output(const string& tag, int icycle)

}; // end of template <class Toa> class HypsiOutputAgent



} // end namespace HYPSI


#endif // _HYPSI_OUTPUT_H_
