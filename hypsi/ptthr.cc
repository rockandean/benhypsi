#include "ptthr.h"
#include "rng.h"

namespace HYPSI {

using namespace std;

// declaration of static variables

PTTHR::State PTTHR::_thrfn_xstate;
PTTHR* PTTHR::_thrfn_ptthr_p;


PTTHR::PTTHR( PTTHR_ActionFunctor* ptaf_p, int hostzone )
  : _ptaf_p(ptaf_p), _hostzone( hostzone )
{
  _state = THR_STATE_NOT_READY;

// following always returns 0
  pthread_mutex_init( &_mutex, NULL );

  pthread_cond_init( &_cond, NULL );

}

void* PTTHR::PTTHR_FN( void *p )
{
/*
  State xstate;

  PTTHR *ptthr_p = static_cast <PTTHR *>(p);
*/

  _thrfn_ptthr_p = static_cast <PTTHR *>(p);

#ifdef DIAG_PTTHR_L2
  cout << "Z" << _thrfn_ptthr_p->hostzone()
   << " PTTHR_FN: about to lock, & set state READY" << endl;
#endif
  
  _thrfn_ptthr_p->lock_mutex();
  
  if( _thrfn_ptthr_p->_state != THR_STATE_NOT_READY )
    throw HypsiException( "State not THR_STATE_NOT_READY",
                          "PTTHR::PTTHR_FN" );
  
  _thrfn_ptthr_p->_state = THR_STATE_READY;
  
#ifdef DIAG_PTTHR_L2
cout << "Z" << _thrfn_ptthr_p->hostzone() << " PTTHR_FN: about to cond_broadcast" << endl;
#endif
  _thrfn_ptthr_p->cond_broadcast();
  
  _thrfn_ptthr_p->unlock_mutex();

  while( true )
  {

    _thrfn_ptthr_p->lock_mutex();

    if( !( _thrfn_ptthr_p->_state == THR_STATE_WORKING
           || _thrfn_ptthr_p->_state == THR_STATE_READY ) )
      throw HypsiException(
              "State not THR_STATE_WORKING or THR_STATE_READY",
              "PTTHR::PTTHR_FN" );

    if( _thrfn_ptthr_p->_state == THR_STATE_WORKING )
      _thrfn_ptthr_p->_state = THR_STATE_WORK_DONE;

#ifdef DIAG_PTTHR_L2
cout << "Z" << _thrfn_ptthr_p->hostzone() << " PTTHR_FN: about to cond_wait" << endl;
#endif

    _thrfn_ptthr_p->cond_wait( );

#ifdef DIAG_PTTHR_L2
cout << "Z" << _thrfn_ptthr_p->hostzone()
     << " PTTHR_FN: returned from cond_wait: state: "
     << _thrfn_ptthr_p->_state << endl;
#endif

    _thrfn_xstate = _thrfn_ptthr_p->_state;
    if( _thrfn_xstate == THR_STATE_REQUEST_EXIT )
    {

      _thrfn_ptthr_p->_state = THR_STATE_EXITING;
      break;

    } else if( _thrfn_xstate == THR_STATE_START_WORK )
    {
#ifdef DIAG_PTTHR_L2
cout << "Z" << _thrfn_ptthr_p->hostzone() << " PTTHR_FN: THR_STATE_START_WORK" << endl;
#endif
      _thrfn_ptthr_p->_state = THR_STATE_WORKING;

    } else
    {
      throw HypsiException(
              "State not THR_STATE_REQUEST_EXIT or THR_STATE_START_WORK",
              "PTTHR::PTTHR_FN" );
    }

    _thrfn_ptthr_p->unlock_mutex();

    if( _thrfn_xstate == THR_STATE_START_WORK )
    {
#ifdef DIAG_PTTHR_L2
cout << "Z" << _thrfn_ptthr_p->hostzone()
     << " PTTHR_FN: Action! about to call action() on PTAF type: "
     << _thrfn_ptthr_p->_ptaf_p->type() << endl;
#endif
    
       _thrfn_ptthr_p->_ptaf_p->action();

     } else
     {
      throw HypsiException(
              "xstate not THR_STATE_START_WORK - shouldnotbehere!",
              "PTTHR::PTTHR_FN" );
     }

  }
  
  _thrfn_ptthr_p->unlock_mutex();

#ifdef DIAG_PTTHR_L2
  cout << "Z" << _thrfn_ptthr_p->hostzone() << "PTTHR_FN: about to exit" << endl;
#endif

  pthread_exit((void*) 0);

}


void PTTHR::thread_create( void )
{
  int errval = pthread_create( &_ptthr, NULL, PTTHR_FN, (void*)this );
  if( errval )
    throw HypsiException( "", "PTTHR::thread_create", errval );
}



void PTTHR::lock_mutex( void )
{
  int errval = pthread_mutex_lock( &_mutex );
  if( errval == EINVAL || errval == EDEADLK )
    throw HypsiException( "", "HYPSI::PTTHR::lock_mutex", errval );
}

bool PTTHR::trylock_mutex( void )
{
  int errval = pthread_mutex_trylock( &_mutex );
  if( errval == EINVAL )
    throw HypsiException( "", "HYPSI::PTTHR::trylock_mutex", errval );
  return errval != EBUSY;
}

void PTTHR::unlock_mutex( void )
{
  int errval = pthread_mutex_unlock( &_mutex );
  if( errval == EINVAL || errval == EPERM )
    throw HypsiException( "", "HYPSI::PTTHR::unlock_mutex", errval );
}

void PTTHR::cond_wait( void )
{
  int errval = pthread_cond_wait( &_cond, &_mutex );
  if( errval == EINVAL || errval == EPERM )
    throw HypsiException( "", "HYPSI::PTTHR::cond_wait", errval );
}

void PTTHR::cond_broadcast( void )
{
  int errval = pthread_cond_broadcast( &_cond );
  if( errval == EINVAL || errval == EPERM )
    throw HypsiException( "", "HYPSI::PTTHR::cond_broadcast", errval );
}

void PTTHR::cond_signal( void )
{
  int errval = pthread_cond_signal( &_cond );
  if( errval == EINVAL || errval == EPERM )
    throw HypsiException( "", "HYPSI::PTTHR::cond_signal", errval );
}

void PTTHR::thread_join( void )
{
  int errval = pthread_join( _ptthr, NULL );
  if( errval )
    throw HypsiException( "", "HYPSI::PTTHR::thread_join", errval );
}

} // end namespace HYPSI

