#ifndef _BLOBS3D_H_
#define _BLOBS3D_H_

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

namespace HYPSI {

class Blobs3dIC
{
/*
  int nx,nx2;
  int ny,ny2;
  int nz,nz2;
  int nyz2;
*/
public:

    vector<double>  blob_center;
    double          blob_width;
    UniformIC       uniform_initial_conditions;

    Blobs3dIC(void){;}

    void read_input_data( ConfigData& config_data);
    void initialize_uniform_params( Hypsim& hypsim );
    void pmove_fields_and_particles( Hypsim& hypsim);
/*
    inline int idx( int ix, int iy, int iz ) const
    {
    nx2 = nx+2;
    ny2 = ny+2;
    nz2 = nz+2;
    nyz2=ny2*nz2;
    return ix*nyz2 + iy*nz2 + iz; }*/
};

} // end namespace HIPSY
#endif // BLOBS3D_H
