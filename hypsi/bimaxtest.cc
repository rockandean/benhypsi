#include "hypsi.h"

#include "mdot/Histo.h"

using namespace HYPSI;

main()
{
// random number generator
  RNG prng;
  prng.set_seed( -12351 );


  ParticleSet pset( 8000000 );  // space for 2 10^6 particles
  
  
  int np = 8000000;   // number of particle

  double vthpar = 1.0;
  double vthperp = 0.5;
  
  xyzVector Bvec( 1.0, 0.0, 0.0 );
  xyzVector Bhatvec( Bvec );
  Bhatvec.normalize();

  xyzRegion xyz_region( 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 );
  
  double Vparshift = 0.0;
  xyzVector Vshift( 0.0, 0.0, 0.0 );
  
  pidxDoublet pidx_range;

  pset.initialize_uniform_bimax(
    np,
    &prng,
    xyz_region,
    vthpar,
    vthperp,
    Bvec,
    Vparshift,
    Vshift,
    pidx_range );

  cout << "Added particles: " << pidx_range.first 
       << " -> " << pidx_range.last << "\n";

  MDOT::Histo vpar_histo;
  MDOT::Histo vperp_histo;

  vpar_histo.set_linear_bin_ranges( 100, -5.0, +5.0 );

  vperp_histo.set_linear_bin_ranges( 50, 0.0, +5.0 );

  vector<double> vpar_histodata(np);
  vector<double> vperp_histodata(np);

  double vpar_av = 0.0, vperp_av = 0.0;

  for( int i=0; i< np ; ++i )
  {
    xyzVector vxyz( pset.pdata[i*6+3], pset.pdata[i*6+4], pset.pdata[i*6+5] );

    vpar_histodata[i] = dot( vxyz, Bhatvec );
  
    xyzVector vperp( vxyz._x - vpar_histodata[i] * Bhatvec._x,
                     vxyz._y - vpar_histodata[i] * Bhatvec._y,
                     vxyz._z - vpar_histodata[i] * Bhatvec._z );
    
    vperp_histodata[i] = vperp.mag();

    vperp_av += vperp_histodata[i];
    vpar_av += vpar_histodata[i];
 
  }

  vperp_av /= np;
  vpar_av /= np;

  cout << " Vpar average: " << vpar_av << ",  Vperp average: "
       << vperp_av << "\n";

  try{

  vpar_histo.accumulate( vpar_histodata );
  vperp_histo.accumulate( vperp_histodata );

//  vpar_histo.diag_cout();
//  vperp_histo.diag_cout();

  } catch( MDOT::MdGeneralException e )
  {
    e.diag_cout();
  }

  vector<MDOT::HistoBinRange> bin_ranges;
  vector<int> bin_counts;
  
  vpar_histo.get_bin_ranges( bin_ranges );
  vpar_histo.get_bin_counts( bin_counts );

  cout << " ======================================================\n";
  cout << " V par  distribution ==================================\n";

  for( int i=0; i<bin_ranges.size(); ++i )
  {
    double np_in_range = 0.5*np*( erf( bin_ranges[i].upper()/vthpar )
                 - erf( bin_ranges[i].lower()/vthpar ) );
    
    double errpc = static_cast<int>(
                 10000*(bin_counts[i]-np_in_range)/np_in_range) / 100.0;
    
    cout << i << ":   "
     << bin_counts[i]
     << "  Exp: " 
     << np_in_range
     << "  Error: "
     << errpc << " %"
     << "\n";
  
  }
  
  vperp_histo.get_bin_ranges( bin_ranges );
  vperp_histo.get_bin_counts( bin_counts );

  cout << " ======================================================\n";
  cout << " V perp distribution ==================================\n";

  for( int i=0; i<bin_ranges.size(); ++i )
  {
    double vperp2 = bin_ranges[i].upper()/vthperp;
    double vperp1 = bin_ranges[i].lower()/vthperp;
    double np_in_range = np*( exp( -vperp1*vperp1 )
                          - exp( -vperp2*vperp2 ) );
    
    double errpc = static_cast<int>(
                 10000*(bin_counts[i]-np_in_range)/np_in_range) / 100.0;
    
    cout << i << ":   "
     << bin_counts[i]
     << "  Exp: " 
     << np_in_range
     << "  Error: "
     << errpc << " %"
     << "\n";
  
  }
  
  
}
