#include <iostream>

#include "hypsi.h"
#include "output.h"
#include "hdf5adaptor.h"
#include "alfvenwic.h"

using namespace std;
using namespace HYPSI;

main( int argc, char** argv )
{
  Timer timer;

  try
  {

  Hypsim hypsim;

//  UniformIC uniform_initial_conditions;

//  AlfvenWaveIC alfvenwave_initial_conditions;

  hypsim.mpi_comm.initialize( &argc, &argv );
  
  cout << "Number of nodes: " << hypsim.mpi_comm.nnodes << "\n";
  cout << "This node: " << hypsim.mpi_comm.thisnode << "\n";
  
  hypsim.initialize( argc, argv );
  
  hypsim.zinfo.print( cout, "hypsi_main: This zone ... " );


  } catch( HypsiException& e )
  {
    cout << "HYPSI Exception at main level\n";
    e.diag_cout();
    cout << endl;
  }
  
}
