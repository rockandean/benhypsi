/* \file
Header file for PTTHR class
*/

#ifndef _HYPSI_PTTHR_H_
#define _HYPSI_PTTHR_H_

#include <iostream>
#include <errno.h>
#include <pthread.h>

#include "exception.h"

namespace HYPSI {

using namespace std;

/*! \brief base class for action within the particle thread */

class PTTHR_ActionFunctor {
public:
  PTTHR_ActionFunctor( void ) {;}

  virtual void action( void ) = 0;
  virtual string type( void ) = 0;

};

// ------------------------------------------------------ forward declarations

class Zone;
class ParticleSet;
class ZoneFields;
class PmoveZMoments;
class NVTMoments;
class ParticleBCFunctor;

// ---------------------------------------------------------------------------


class PTAF_pmove : public PTTHR_ActionFunctor {

  ParticleSet* _psetp;
  ParticleBCFunctor* _bcfp;
  int _izone;
  int _hostzone;
  double _dt, _dth, _dta;  //< timestep variables

public:
  ZoneFields* _zfields;
  PmoveZMoments* _pzmoms;

public:
  PTAF_pmove( const Zone& zoneinfo );
  ~PTAF_pmove( void );

  void action( void );
  string type( void ) { return "PTTHR_ActionFunctor::PTAF_pmove"; }
  
  void set_psetp_dt( ParticleSet* psetp, double dt );
  void set_zone( int izone ) { _izone = izone; }
  void set_hostzone( int iz ) { _hostzone = iz; }
  void set_particle_bcfp( ParticleBCFunctor* bcfp ) { _bcfp = bcfp; }
};

// ------------------------------------------------------------ PTAF_collectnv

class PTAF_collectnvt : public  PTTHR_ActionFunctor {

  ParticleSet* _psetp;
  Zone* _zone;
  int _zoid;
  int _hostzone;
  bool _collectT;  // true if collecting temperature

public:
  NVTMoments* nvtmoms;

public:

  PTAF_collectnvt( const Zone& zone );

  ~PTAF_collectnvt( void );

  void action( void );

  string type( void ) { return "PTTHR_ActionFunctor::PTAF_collectnvt"; }
  
  void set_psetp( ParticleSet* psetp ) { _psetp = psetp; }
  void set_zone( const Zone& zone );
  void set_hostzone_id( int iz ) { _hostzone = iz; }
  void set_collectT( bool collectT ) { _collectT = collectT; }
};

// -------------------------------------------------------------------- PTTHR

class PTTHR {
private:
  pthread_t _ptthr;
  pthread_mutex_t _mutex;
  pthread_cond_t _cond;

  PTTHR_ActionFunctor* _ptaf_p;  //< pointer to action function
  
  int _hostzone;

public:

  enum State { THR_STATE_NOT_READY, THR_STATE_READY,
               THR_STATE_START_WORK,
               THR_STATE_WORKING, THR_STATE_WORK_DONE,
               THR_STATE_REQUEST_EXIT, THR_STATE_EXITING };
  State _state;

// static variables used by PTTHR_FN

  static State _thrfn_xstate;
  static PTTHR *_thrfn_ptthr_p;

public:

  PTTHR( PTTHR_ActionFunctor* ptaf_p, int hostzone );

  static void* PTTHR_FN( void *p );
  
  void thread_create( void );
  void lock_mutex( void );
  bool trylock_mutex( void );
  void unlock_mutex( void );
  void cond_wait( void );
  void cond_broadcast( void );
  void cond_signal( void );
  void thread_join( void );

  void set_state( State s ) { _state = s; }
  State state( void ) const { return _state; }
  
  int hostzone() const { return _hostzone; }

  static void ssleep( double sec )
  { struct timespec mytime, myremtime;
    int retval;
    mytime.tv_sec = static_cast<int>(sec);
    mytime.tv_nsec = static_cast<int>((sec-mytime.tv_sec)*1000000000);
    myremtime.tv_sec = 0; myremtime.tv_nsec = 0;
    retval = nanosleep( &mytime, &myremtime );
  }
};

} // end namespace HYPSI


#endif  // #ifndef _HYPSI_PTTHR_H_
