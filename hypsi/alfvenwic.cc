#include "hypsi.h"
#include "alfvenwic.h"

namespace HYPSI {

void AlfvenWaveIC::read_input_data( ConfigData& config_data )
{

// Start off by reading Uniform Plasma initial conditions

  try{

  uniform_initial_conditions.read_input_data( config_data );

  } catch( HypsiException& e )
  {
    e.push_err_msg(
   "In AlfvenWaveIC::read_input_data while reading UNIFORM PLASMA input data" );
    throw e;
  }


  vector<double> modenums;
  vector<double> aplus_inp;
  vector<double> aminus_inp;

  try{

    config_data.get_data(
      "alfven_wave_initial_conditions/mode_numbers", modenums );
    config_data.get_data(
      "alfven_wave_initial_conditions/propagation_direction", _propsign );
    config_data.get_data(
      "alfven_wave_initial_conditions/aplus_cmplxamp", aplus_inp );
    config_data.get_data(
      "alfven_wave_initial_conditions/aminus_cmplxamp", aminus_inp );

  } catch(  KVF::kvfException& e )
  {
    e.diag_cout();
    throw HypsiException(
      "Failed to get Alfven Wave IC data from file: <"
         + config_data.filename() + ">",
      "AlvenWaveIC::initialize_from_config_data" );
  }

  if( !( _propsign == +1 || _propsign == -1 ) )
    throw HypsiException(
      "alfven_wave_initial_conditions/propagation_direction must be +/- 1 in file: <"
         + config_data.filename() + ">",
      "AlvenWaveIC::initialize_from_config_data" );

  if( aplus_inp.size() != 2 || aminus_inp.size() != 2 )
    throw HypsiException(
      "Not a two number array for aplus_cmplxamp and/or aminus_cmplxamp in file: <"
         + config_data.filename() + ">",
      "AlvenWaveIC::initialize_from_config_data" );

  _aplus_amplitude = complex( aplus_inp[0], aplus_inp[1] );
  _aminus_amplitude = complex( aminus_inp[0], aminus_inp[1] );

  if( modenums.size() != 3 )
    throw HypsiException(
      "Insufficient data for mode_numbers array in file: <"
         + config_data.filename() + ">",
      "AlvenWaveIC::initialize_from_config_data" );
  
  _mx = static_cast<int>( modenums[0] );
  _my = static_cast<int>( modenums[1] );
  _mz = static_cast<int>( modenums[2] );

  if( _mx != modenums[0] || _my != modenums[1] || _mz != modenums[2] )
    throw HypsiException(
      "Some mode_numbers not integer in file: <" + config_data.filename() + ">",
      "AlvenWaveIC::initialize_from_config_data" );


} // end AlfvenWaveIC::read_input_data


void AlfvenWaveIC::initialize_fields_and_particles( Hypsim& hypsim )
{
// from math.h for double
  const double sqrt2 = 1.41421356237309504880;
  const double twopi = 2.0 * 3.14159265358979323846;

// FIRST CALCULATE QUANTITIES, k vector, unit vectors for wave components etc


  _kvec.set( twopi*_mx/hypsim.sim_params.domain_region.xlen(),
             twopi*_my/hypsim.sim_params.domain_region.ylen(),
             twopi*_mz/hypsim.sim_params.domain_region.zlen() );
  
  _Bhatvec.set( _kvec._x, _kvec._y, _kvec._z );
  
  _Bhatvec.normalize();

  if( _Bhatvec._x == 0.0 && _Bhatvec._y == 0.0 )
  {
    _Bperp1vec.set( _Bhatvec._z, 0.0,  -_Bhatvec._x );
  
    _Bperp1vec.normalize();
  
    _Bperp2vec.set( _Bhatvec._y*_Bperp1vec._z - _Bhatvec._z*_Bperp1vec._y,
                    _Bhatvec._z*_Bperp1vec._x - _Bhatvec._x*_Bperp1vec._z,
                    _Bhatvec._x*_Bperp1vec._y - _Bhatvec._y*_Bperp1vec._x );

    _Bperp2vec.normalize();
  
  } else
  {
    _Bperp1vec.set( -_Bhatvec._y, _Bhatvec._x, 0.0 );
  
    _Bperp1vec.normalize();
  
    _Bperp2vec.set( _Bhatvec._y*_Bperp1vec._z - _Bhatvec._z*_Bperp1vec._y,
                    _Bhatvec._z*_Bperp1vec._x - _Bhatvec._x*_Bperp1vec._z,
                    _Bhatvec._x*_Bperp1vec._y - _Bhatvec._y*_Bperp1vec._x );

    _Bperp2vec.normalize();
  }

/*
  cout << "k vector" << _kvec << " Propagation sign: " << _propsign 
       << " LH: "<< _aplus_amplitude<<" RH: " << _aminus_amplitude << "\n";
  cout << "B unit vectors: Bpar : " << _Bhatvec << " Bper1: " << _Bperp1vec
       << " Bper2: " << _Bperp2vec << "\n";
  
*/

// NEXT ... initialize UNIFORM PLASMA initial conditions

// uniform B field from calculation above, ie // to k vector

  hypsim.initialize_uniform_B( _Bhatvec );
  
  hypsim.initialize_uniform_resis( hypsim.sim_params.uniform_resis );
  hypsim.initialize_pe( hypsim.sim_params.initial_Te );

// override the B vector from input data for UniformPlasma, so that
//  Tpar, Tperp is consistent with Alfven wave B vector

  uniform_initial_conditions._Bvec = _Bhatvec;

//cout << "Z" << hypsim.mpi_comm.thisnode << ": alf ic: About to initialize_particle_sets \n";

// cellrandom

  uniform_initial_conditions.initialize_particle_sets( hypsim );

// zonerandom
//  uniform_initial_conditions.initialize_particle_sets_zonerandom( hypsim );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": alf ic: Done initialize_particle_sets \n";

// LOOP over all cells

  for( int ix = 1; ix <= hypsim.zinfo.nx; ++ix )
  for( int iy = 1; iy <= hypsim.zinfo.ny; ++iy )
  for( int iz = 1; iz <= hypsim.zinfo.nz; ++iz )
  {
    complex cisphase;
    xyzVector pos;
    double a1, a2, phase;

// ixcn etc is host posintion in compute node space
    const int host_ix_offset = hypsim.zinfo.ixcn * hypsim.zinfo.nx;
    const int host_iy_offset = hypsim.zinfo.iycn * hypsim.zinfo.ny;
    const int host_iz_offset = hypsim.zinfo.izcn * hypsim.zinfo.nz;


    int ixyz;

    hypsim.domain_info.get_cellvertex_position( host_ix_offset + ix,
                                                host_iy_offset + iy,
                                                host_iz_offset + iz,
                                                pos );

    phase = dot( _kvec, pos );

    cisphase = complex( cos( phase ), sin( phase ) );
    
    a1 = real( cisphase * ( _aplus_amplitude + _aminus_amplitude ) )/ sqrt2;
    a2 = real( complex(0.0,1.0) *
                cisphase * ( _aplus_amplitude - _aminus_amplitude ) )/ sqrt2;

// modify the field values

    ixyz = hypsim.zfields->idx(ix,iy,iz);

    hypsim.zfields->Bx[ ixyz ] += a1 * _Bperp1vec._x + a2 * _Bperp2vec._x;
    hypsim.zfields->By[ ixyz ] += a1 * _Bperp1vec._y + a2 * _Bperp2vec._y;
    hypsim.zfields->Bz[ ixyz ] += a1 * _Bperp1vec._z + a2 * _Bperp2vec._z;
 
  }

//cout << "Z" << hypsim.mpi_comm.thisnode << ": alf ic: About to make_B_xyzperiodic \n";

  MPI_Barrier( MPI_COMM_WORLD );

//cout << "Z" << hypsim.mpi_comm.thisnode << ": alf ic: after Barrier: About to make_B_xyzperiodic \n";

  hypsim.zfields->make_B_xyzperiodic();
  
//cout << "Z" << hypsim.mpi_comm.thisnode << ": alf ic: Done make_B_xyzperiodic \n";

  hypsim.zhfields->copy_Bdata( *(hypsim.zfields) );

// loop over all the particles

// JUST USE PARTICLE SET ZERO ...(at the moment)

// assumes B_0 = 1, and V_A = 1 (completely protonic)

  for(int ip=0; ip<hypsim.psets[0]->np; ++ip )
  {
    complex cisphase;
    xyzVector pos;
    double a1, a2, phase;
    double *pp;
    
    pp = hypsim.psets[0]->pdata +ip*6;

    pos.set( pp[0], pp[1], pp[2] );

    phase = dot( _kvec, pos );

    cisphase = complex( cos( phase ), sin( phase ) );
    
    a1 = real( cisphase * ( _aplus_amplitude + _aminus_amplitude ) )/ sqrt2;
    a2 = real( complex(0.0,1.0) *
                cisphase * ( _aplus_amplitude - _aminus_amplitude ) )/ sqrt2;

// modify the field values

    pp[3] -= _propsign*( a1 * _Bperp1vec._x + a2 * _Bperp2vec._x );
    pp[4] -= _propsign*( a1 * _Bperp1vec._y + a2 * _Bperp2vec._y );
    pp[5] -= _propsign*( a1 * _Bperp1vec._z + a2 * _Bperp2vec._z );
  }

}


} // end namespace HYPSI
