#include <iostream>
#include "ptthr.h"

using namespace std;
using namespace HYPSI;

main()
{
  struct timespec mytime, myremtime;
  int retval;

  cout << "This is main thread" << endl;

  PTTHR ptthr( new PTAF_Test() );

  ptthr.set_state( PTTHR::STATE_NOT_READY );

  cout << "MAIN: about to lock" << endl;
  ptthr.lock_mutex();

  cout << "MAIN: about to thread_create()" << endl;
  ptthr.thread_create();

  cout << "This is main thread again" << endl;
  
  cout << "MAIN: wait until state READY" << endl;

  int ic=0;
  while( ptthr.state() != PTTHR::STATE_READY )
  {
    ptthr.unlock_mutex();
    ic++;
    cout << "MAIN:  .. wait 0.001 s" << endl;
    ptthr.ssleep( 0.001 );
    ptthr.lock_mutex();
  }

   cout << "MAIN: state now READY (while{} count: " << ic << ")" << endl;
 

 /*     cout << "MAIN:  .. wait 0.5 s" << endl;
      mytime.tv_sec = 0; mytime.tv_nsec = 500000000;
      myremtime.tv_sec = 0; myremtime.tv_nsec = 0;
      retval = nanosleep( &mytime, &myremtime );
*/

  cout << "The particle thread will be WORKed 3 times and then EXIT" << endl;

  int icount = 0;
  while( true )
  {

/*  cout << "MAIN: about to lock" << endl;
  ptthr.lock_mutex(); */

// ptcle op should really be set in this locked section!

  if( icount++ < 3 )
    ptthr.set_state( PTTHR::STATE_WORK_TODO );
  else
    ptthr.set_state( PTTHR::STATE_REQUEST_EXIT );

  cout << "MAIN: about to cond_signal" << endl;
  ptthr.cond_broadcast( );
  cout << "MAIN: about to unlock" << endl;
  ptthr.unlock_mutex();
  
  

  while( true )
  {
    cout << "MAIN: about to trylock" << endl;
    if( ptthr.trylock_mutex() )
    {
      cout << "MAIN: trylock successful" << endl;

      if( ptthr.state() == PTTHR::STATE_WORK_DONE )
      {

         cout << "MAIN: about to unlock" << endl;
         ptthr.unlock_mutex();

cout << "MAIN: PTTHR::STATE_WORK_DONE .. break & continue" << endl;
         break;
      } else if( ptthr.state() == PTTHR::STATE_EXITING )
      {

         cout << "MAIN: about to unlock" << endl;
         ptthr.unlock_mutex();

cout << "MAIN: PTTHR::STATE_EXITING .. break & exit loop" << endl;
         break;
      
      } else 
      {
        cout << "MAIN: about to unlock" << endl;
        ptthr.unlock_mutex();
        cout << "MAIN: != PTTHR::STATE_WORK_DONE .. wait 0.1 s" << endl;
        mytime.tv_sec = 0; mytime.tv_nsec = 100000000;
        myremtime.tv_sec = 0; myremtime.tv_nsec = 0;
        retval = nanosleep( &mytime, &myremtime );
      }
    
    } else 
    {
      cout << "MAIN: trylock unsuccessful .. wait 0.5 s" << endl;
      
      ptthr.ssleep( 0.5 );

    }
  }

  cout << "MAIN: about to lock" << endl;
  ptthr.lock_mutex();
 

  if( ptthr.state() == PTTHR::STATE_EXITING )
    break;
  }
  
  cout << "Main exited from loop" << endl;

  cout << "Main thread about to unlock" << endl;
  ptthr.unlock_mutex();

  cout << "Main thread about to nanosleep for 1sec" << endl;
  ptthr.ssleep( 1 );

  cout << "Main thread about to exit" << endl;

}
