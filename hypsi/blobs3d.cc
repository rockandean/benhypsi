
#include "hypsi.h"
#include "blobs3d.h"
#include<iostream>
#include <cmath>

namespace HYPSI {

void Blobs3dIC::read_input_data( ConfigData& config_data)
{
    // Start off by reading Uniform Plasma initial conditions
    try{ uniform_initial_conditions.read_input_data( config_data );
    }
    catch (HypsiException& e){
        e.push_err_msg("In Blobs3d::read_input_data while reading UNIFORM PLASMA input data");
        throw e;
    }

    // get parameters from file
    try{config_data.get_data("blobs3d/blob_center", blob_center);
        config_data.get_data("blobs3d/blob_width" , blob_width );
    }
    catch (KVF::kvfException& e){
        e.diag_cout();
        throw HypsiException("Failed to get Blob Initialize from data file: <"
                             + config_data.filename()+ ">",
                             "blobs3d::read_input_data config_data.get_data");
    }
    if( blob_center.size() != 2 )
        throw HypsiException(
      "Inadequate data for blobs3d/blob_center  in file: <" + config_data.filename() + ">",
      "Blobs3dIC::read_input_data" );
    if( blob_width < 0.0 )
        throw HypsiException(
      "Inadequate data for blobs3d/blob_width (must > 0) in file: <" + config_data.filename() + ">",
      "Blobs3dIC::read_input_data" );

} //--------------------------------------------- end Blobs3dIC::read_input_data


void Blobs3dIC::initialize_uniform_params( Hypsim& hypsim )
{
  hypsim.initialize_uniform_B       ( uniform_initial_conditions._Bvec );
  hypsim.initialize_uniform_resis   ( hypsim.sim_params.uniform_resis );
  hypsim.initialize_pe              ( hypsim.sim_params.initial_Te );

  //! todo meantime using the fix directly in uniformIC
  // pass blob values here
  uniform_initial_conditions._position_centre.assign(3,0.0);
  uniform_initial_conditions._position_centre[0]= blob_center[0];
  uniform_initial_conditions._position_centre[1]= blob_center[1];
  uniform_initial_conditions._position_centre[2]= blob_width;
  uniform_initial_conditions.initialize_particle_sets( hypsim );
  /*
    if ( _force_free == 1 ) { //! \todo use proper selection here
        initialize_particle_sets_forcefree( hypsim );
    } else {
        initialize_particle_sets_harris   ( hypsim );
    }
  */
  // zone random: uniform_initial_conditions.initialize_particle_sets_zonerandom( hypsim );

}//--------------------------------------- end Blobs3dIC::initialize_uniform_params


void Blobs3dIC::pmove_fields_and_particles(Hypsim& hypsim){
    
    //sim_params.npsets is not there when uniformIC is not ins use
    for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset ){
    
    //cout<< "Z" << hypsim.mpi_comm.thisnode<<": SORTING .. Particle set: " << ipset << "\n";
    hypsim.psets[ipset]->sort_by_zone( hypsim.domain_info, hypsim.zones_info );
    // cout<< "Z" << hypsim.mpi_comm.thisnode<<": ACTIVE ZONES: "
    //<< hypsim.psets[ipset]->n_active_zones << "\n";
    }

    hypsim.pmove( 0.0 );
    MPI_Barrier( MPI_COMM_WORLD );

    /*// density moments, this may be the same as in nvt, but I need to check.
          i)- test if dnB does output
          ii) - compare pzmoms with nvtmoms
          ii) choose appropiate to use in balance force

    // Need to calculate thse moments
      for( int ipset=0; ipset<hypsim.sim_params.npsets; ++ipset ){

        //cout<< "Z" << hypsim.mpi_comm.thisnode<<"MAKE PERIODIC ipset: " << ipset << "\n";
        hypsim.pset_pzmoms[ipset]->make_xyzperiodic();
      }

      hypsim.calc_nvq_moments();
      //cout<< "Z" << hypsim.mpi_comm.thisnode<<"COMPLETED calc_nvq_moments:\n";
    */

    hypsim.allocate_pset_nvtmoms();
    hypsim.collect_nvtmoms_xyzperiodic( true);

    if (hypsim.mpi_comm.thisnode==0)
        std::cout<<" ****HERE blob3d.cc*******\n ********** force balance *********"<<endl;
    // THE FORCE BALANCE TO THE FIELDS HERE
    // Loop over all cells

    int nyz2= hypsim.zinfo.nyz2;
    int nz2 = hypsim.zinfo.nz2;

    int myindex=0;
    double discriminant(0.0);
    double Pelectron(0.0);
    int ixyz(0);

    for( int ix = 1; ix <= hypsim.zinfo.nx; ++ix )
    for( int iy = 1; iy <= hypsim.zinfo.ny; ++iy )
    for( int iz = 1; iz <= hypsim.zinfo.nz; ++iz )
    {
 

    ixyz = hypsim.zfields->idx(ix,iy,iz);
    /*
       double NZ(0.0), Nsum(0.0), vy2(0.0), vz2(0.0), pressureIons(0.0);

        for ( int i=0; i<=hypsim.sim_params.npsets; ++i){
          vy2 += hypsim.pset_nvtmoms[i]->Vy2B[ixyz];
          vz2 += hypsim.pset_nvtmoms[i]->Vz2B[ixyz];
        }
        // n_blob* temp
        pressureIons = hypsim.pset_nvtmoms[1]->dnB[ixyz]*hypsim.sim_params.psmass[1]*( vy2 + vz2 )/3.0;
    */
    Pelectron = pow(hypsim.pset_nvtmoms[1]->dnB[ixyz],hypsim.sim_params.electron_gamma)*hypsim.sim_params.initial_Te;
    


    discriminant   = hypsim.zfields->Bx[ ixyz ]*hypsim.zfields->Bx[ ixyz ]-2.0*Pelectron;

    if (discriminant < 0.00 && hypsim.mpi_comm.thisnode==20 && myindex < 10){
        std::cout<< "B^2 is negative: discriminant "<<discriminant<<std::endl;
        std::cout<<"FieldZ "<<hypsim.zfields->Bx[ ixyz ]<<"\n Pelectron "<<Pelectron<<endl;
        std::cout<<"ixyz: "<< ixyz <<endl;
        myindex++;
    }
  
    hypsim.zfields->Bx[ ixyz ] = sqrt(abs(discriminant));
    

    }

    hypsim.free_pset_nvtmoms();


    //cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: after Barrier: About to make_B_xyzperiodic \n";

    hypsim.zfields->make_B_xyzperiodic();

    //cout << "Z" << hypsim.mpi_comm.thisnode << ": disc ic: Done make_B_xyzperiodic \n";

    hypsim.zhfields->copy_Bdata( *(hypsim.zfields) );
    //
        
} //--------------------------------- end Blobs3dIC::initialize_fields_and_particles


} // end namespace HIPSY
